#!/bin/bash

# man null
# man cut
# man sed

echo "
### Liens utiles

- [Jeux de données _open data_ sur l'enseignement supérieur et la recherche](https://data.enseignementsup-recherche.gouv.fr)
- [Insertion professionnelle des diplômés de Master en universités et établissements assimilés - données nationales par disciplines détaillées](https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-insertion_professionnelle-master_donnees_nationales/table/?refine.annee=2020&refine.disciplines=Sciences,+technologies+et+sant%C3%A9&sort=code_du_secteur_disciplinaire)
  - [Sciences, technologies et santé, 2020](https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-insertion_professionnelle-master_donnees_nationales/table/?refine.annee$&sort=code_du_secteur_disciplinaire&refine.annee=2020&refine.disciplines=Sciences,+technologies+et+sant%C3%A9)
    - [Format CSV](https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-insertion_professionnelle-master_donnees_nationales/exports/csv?lang=en&refine=annee%3A%222020%22&refine=disciplines%3A%22Sciences%2C%20technologies%20et%20sant%C3%A9%22&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B)
" > links.md

rm -f table.html

grep -i csv links.md \
  | sed 's/^.*(// ; s/).*$//' \
  | cut -d " " -f 3 \
  | xargs -I % wget -O - % 2>/dev/null \
  | grep -Ei "Année;|informatique|mathématique" \
  | cut -d ";" -f 1,3,11,14,21 \
  | grep -Ev ";ns|;nd" \
  | sed 's/\(^.*Année;.*$\)/# Insertion professionnelle des diplômés de Master\n## Informatique et mathématique\n\n\1|\n|---|---|---|---|---/g ; s/^\([^#]\)/\|\1/ ; s/;/|/g ; s/$/|/' \
  | pandoc -o table.html

firefox table.html & disown

