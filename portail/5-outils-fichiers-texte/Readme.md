> # Manipulation de fichiers de texte
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

# 1. Outils de manipulation de fichiers de texte

L'objet de ces travaux pratiques est de découvrir d'autres outils en ligne de commande pour l'inspection et la manipulation de fichiers. Les concepts vu dans la partie [système de fichiers](../1-fs+terminal/README.md) et ses [compléments](../1-fs+terminal/complements.md) sont considérés
comme acquis.

**{-à faire-}** Compléter (ou réviser, si déjà complétés) le sujet [système de fichiers](../1-fs+terminal/README.md) ainsi que la partie [compléments](../1-fs+terminal/complements.md) du même sujet.


# 2. Échauffement

## 2.1 Préparation du répertoire de travail

**{-à faire-}** À l'aide des commandes `mkdir` et `cd` créer un répertoire de travail nommé `manipulation_de_fichiers` et s'y placer au terminal.

**{-à faire-}** Télécharger à l'aide de `wget` et décompresser à l'aide de `gunzip` le fichier qui se trouve à cette adresse : `https://gitlab-fil.univ-lille.fr/ls1-odi/portail/-/raw/master/5-outils-fichiers-texte/dictionnaire-fr.txt.gz`.

**{-à faire-}** À l'aide de la commande `ls -l`, afficher le contenu du dossier courant.


## 2.2 Commandes de base 

### 2.2.1 La commande `file`

La commande `file` permet d'avoir des informations sur le type de contenu d'un fichier.

**{-à faire-}** Quel est le type du fichier `dictionnaire-fr.txt` selon la commande `file` ?

### 2.2.2 La commande `wc`

La commande `wc` déjà vue [ici](../1-fs+terminal/complements.md) permet de compter le nombre d'octets, mots et lignes d'un fichier de texte, ou alors du texte passé à travers l'entrée
standard.

**{-à faire-}** Consulter la page `man wc` pour comprendre la sortie et les options de la commande `wc`.

**{-à faire-}** Utiliser convenablement la commande `wc` et ses options pour afficher le nombre d'octets du fichier `dictionnaire-fr.txt`.

**{-à faire-}** Similairement, afficher le nombre de mots du fichier `dictionnaire-fr.txt`. Vous devriez trouver autour de 630000 mots.

**{-à faire-}** Toujours grâce à `wc`, afficher le nombre de lignes du fichier `dictionnaire-fr.txt`. 

**{+remarque+}** En général, le nombre de ligne et de mots d'un fichier est bien différent. Pourquoi à votre avis dans ce cas les deux nombres sont égaux ?

### 2.2.3 La commande `head`

La commande `head` permet d'afficher les lignes 1 à 10 (c'est à dire les 10 premières lignes) d'un fichier de texte.

**{-à faire-}** Afficher à l'aide de la commande `head` le lignes 1 à 10 du fichier `dictionnaire-fr.txt` que vous venez de télécharger.

**{-à faire-}** Lire la page du manuel `man head` pour découvrir les options de la commande `head`.

**{-à faire-}** Utiliser convenablement la commande `head` et ses options pour afficher les lignes 1 à 50 du fichier `dictionnaire-fr.txt`.

**{-à faire-}** Utiliser convenablement la commande `head` et ses options pour découvrir le mot du dictionnaire à la ligne 500.

La sortie de la commande `head`, comme celle de toute autre commande, peut être redirigée vers l'entrée d'autres commandes en utilisant le *tube* `|` vu [ici](../1-fs+terminal/complements.md).

**{-à faire-}** Utiliser convenablement les commandes `head`, `wc -l` et le tube `|` pour vérifier que le nombre de lignes affichées par la commande `head` dans les exercices précédents correspond bien à la valeur demandée.

### 2.2.4 La commande `tail`

De manière similaire à la commande `head`, la commande `tail` permet d'afficher les lignes -10 à -1 (les 10 dernières lignes) d'un fichier texte. Comme pour la commande `head`, la page du manuel `man tail` montre ses options.

**{-à faire-}** Utiliser la commande `tail` pour afficher les lignes -10 à -1 du fichier `dictionnaire-fr.txt`.

**{-à faire-}** Utiliser convenablement la commande `tail` et ses options pour afficher les lignes -50 à -1 du même fichier.

**{-à faire-}** Utiliser convenablement les commandes `tail`, `wc -l` et le tube `|` pour vérifier que le nombre de lignes affichées par la commande `tail` dans les exercices précédents correspond bien à la valeur demandée.

**{-à faire-}** Utiliser convenablement les commandes `head`, `tail` et le tube `|` pour afficher les lignes 11 à 20 du fichier `dictionnaire-fr.txt`.

**{-à faire-}** Utiliser convenablement les commandes `head`, `tail` et le tube `|` pour afficher les lignes 51 à 100 du même fichier.

**{-à faire-}** De la même manière, afficher les lignes -20 à -11 du même fichier.

**{-à faire-}** Encore de la même manière, afficher les lignes -100 à -51 du même fichier.

### 2.2.5 La commande `nl`

La commande `nl` permet de numéroter les lignes d'un fichier.

**{-à faire-}** Exécuter la commande `nl dictionnaire-fr.txt | head -n 5`. Comment justifiez-vous la sortie de cette commande, par rapport à l'effet de la commande `nl`, l'effet du tube `|` et l'effet de la commande `head` ?

La redirection de sortie `>` vue [ici](../1-fs+terminal/complements.md) permet de rediriger la sortie standard d'une commande vers un fichier.

**{-à faire-}** Exécuter la commande `nl dictionnaire-fr.txt > dictionnaire-avec-numeros-de-ligne.txt`. Vérifiez l'existence d'un nouveau fichier à l'aide de la commande `ls`.

**{-à faire-}** Compter le nombre de caractères des fichiers `dictionnaire-fr.txt` et `dictionnaire-avec-numeros-de-ligne.txt` à l'aide de la commande `wc`. Y a-t-il une différence ? À votre avis, pourquoi ?

**{-à faire-}** Compter le nombre de lignes des fichiers `dictionnaire-fr.txt` et `dictionnaire-avec-numeros-de-ligne.txt` à l'aide de la commande `wc`. Y a-t-il une différence ? À votre avis, pourquoi ?

**{-à faire-}** Afficher les 5 premières lignes de `dictionnaire-avec-numeros-de-ligne.txt` à l'aide de la commande `head`.

**{-à faire-}** Afficher les 5 dernières lignes du même fichier à l'aide de la commande `tail`.

**{-à faire-}** Afficher les lignes 51 à 100 du même fichier à l'aide des commandes `head`, `tail` et du tube `|`.

**{-à faire-}** En vous basant sur les sorties affichées par les 6 exercices précédents, y a-t-il une bonne correspondance entre les numéros de lignes données par `nl`, le nombre de lignes comptées par `wc` et les numéros de lignes sélectionnées par `head` et `tail` ?

**{-à faire-}** À l'aide des commandes `head`, `tail`, `nl`, du tube `|` et de la redirection de sortie standard `>`, créez 
- un fichier `premieres-8-lignes.txt` qui contient les 8 premières lignes **numérotées** du fichier `dictionnaire-fr.txt` ;
- un fichier `dernieres-8-lignes.txt` qui contient les 8 dernières lignes **numérotées** du fichier `dictionnaire-fr.txt`.

### 2.2.6 La commande `cat`

La commande `cat` déjà vue [ici](../1-fs+terminal/complements.md) permet d'afficher (c'est à dire, de rediriger sur la sortie standard) le contenu d'un ou plusieurs fichiers. Cette commande permet donc de *concaténer* ces contenus.

**{-à faire-}** À l'aide de la commande `cat` appliquée aux deux fichiers crées précédemment et de la redirection de sortie standard `>`, créer un fichier `premieres-et-dernieres-8-lignes.txt` qui contient les 8 premières et les 8 dernières lignes numérotées du fichier `dictionnaire-fr.txt`.

**{-à faire-}** À l'aide de la commande `cat`, afficher à l'écran le contenu du fichier que vous venez de créer.

### 2.2.7 La commande `tac`

La commande `tac` réalise comme `cat` l'affichage du contenu d'un ou plusieurs fichiers, mais à l'envers.

**{-à faire-}** À l'aide de la commande `tac`, afficher à l'écran le contenu du fichier `premieres-et-dernieres-8-lignes.txt`.

### 2.2.8 La commande `shuf`

La commande `shuf` est similaire à `tac` et `cat`, mais réalise l'affichage des lignes dans une séquence aléatoire.

**{-à faire-}** À l'aide de la commande `shuf`, afficher à l'écran le contenu du fichier `premieres-et-dernieres-8-lignes.txt`.

**{-à faire-}** Répéter la même opération. Obtenez-vous le même résultat ?

**{-à faire-}** Exécuter la commande `shuf dictionnaire-fr.txt | head`. Comment justifiez-vous la sortie de cette commande, par rapport à l'effet de la commande `shuf`, l'effet du tube `|` et l'effet de la commande `head` ?

**{-à faire-}** Répéter la même opération. Obtenez-vous le même résultat ?

**{-à faire-}** Utiliser les commandes `shuf`, `head`, le tube `|` et la redirection `>` pour sélectionner au hasard 100 lignes du fichier `dictionnaire-fr.txt` et les sauvegarder dans un fichier nommé `100-mots-au-hasard.txt`.

**{-à faire-}** Afficher le contenu du fichier `100-mots-au-hasard.txt` à l'aide de la commande `cat`.

### 2.2.9 La commande `sort`

La commande `sort` affiche aussi le contenu de fichiers ou de l'entrée standard comme `cat`, `tac` et `shuf`, mais elle trie les lignes selon l'ordre alphanumérique.

**{-à faire-}** Exécuter la commande `sort 100-mots-au-hasard.txt` et la comparer au contenu du même fichier.

**{-à faire-}** À l'aide des commandes et redirections vues précédemment, créer un fichier `dictionnaire-1000-mots-fr.txt` qui contient 1000 mots triés choisis au hasard à partir du fichier `dictionnaire-fr.txt`.


# 3. Outils basés sur les expressions régulières 

Dans la suite, nous allons voir des outils pour l'inspection de fichiers basés sur les *expressions régulières*.

De [Wikipédia](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re#Principes) :

> Une expression régulière (ou _rationnelle_) est une suite de caractères typographiques (qu’on appelle plus simplement « motif » – « pattern » en anglais) décrivant un ensemble de chaînes de caractères. Par exemple l’ensemble de mots « ex-équo, ex-equo, ex-aequo et ex-æquo » peut être condensé en un seul motif « ex-(a?e|æ|é)quo ». Les mécanismes de base pour former de telles expressions sont basés sur des caractères spéciaux de substitution, de groupement et de quantification.
>
> Une barre verticale sépare le plus souvent deux expressions alternatives : « equo|aequo » désigne soit equo, soit aequo. Il est également possible d’utiliser des parenthèses pour définir le champ et la priorité de la détection, « (ae|e)quo » désignant le même ensemble que « aequo|equo » et de quantifier les groupements présents dans le motif en apposant des caractères de quantification à droite de ces groupements.
>
> Les quantificateurs les plus répandus sont :
>
>    - ? qui définit un groupe qui existe zéro ou une fois : toto? correspondant alors à « tot » ou « toto » mais pas « totoo » ;
>    - \* qui définit un groupe qui existe zéro, une ou plusieurs fois (l'étoile de Kleene) : toto* correspondant à « tot », « toto », « totoo », « totooo », etc. ;
>    - \+ qui définit un groupe qui existe une ou plusieurs fois : toto+ correspondant à « toto », « totoo », « totooo », etc. mais pas « tot ».
>
> Les symboles dotés d'une sémantique particulière peuvent être appelés « opérateurs », « métacaractères » ou « caractères spéciaux ». Les caractères qui ne représentent qu'eux-mêmes sont dits « littéraux ».
>
> Les expressions régulières peuvent être combinées, par exemple par concaténation, pour produire des expressions régulières plus complexes.
>
> Lorsqu'une chaîne de caractères correspond à la description donnée par l'expression régulière, on dit qu'il y a « correspondance » entre la chaîne et le motif, ou que le motif « reconnaît » la chaîne. Cette correspondance peut concerner la totalité ou une partie de la chaîne de caractères. Par exemple, dans la phrase « Les deux équipes ont terminé ex-æquo et se sont saluées. », la sous-chaîne « ex-æquo » est reconnue par le motif « ex-(a?e|æ|é)quo ».
>
> Par défaut, les expressions régulières sont sensibles à la casse. Lorsque c'est possible, elles tentent de reconnaître la plus grande sous-chaîne correspondant au motif : on dit qu'elles sont « gourmandes ». Par exemple, Aa+ reconnaît la totalité de la chaîne « Aaaaaaa » plutôt qu'une partie « Aaa » (gourmandise), mais elle ne reconnaît pas la chaîne « aaaA » (sensibilité à la casse). 

## 3.1. La commande grep

De [Wikipédia](https://fr.wikipedia.org/wiki/Grep) :
> **`grep`** est un programme en ligne de commande de recherche de chaînes de caractères.
> Le comportement habituel de grep est de recevoir une expression rationnelle en argument, de lire les données sur l'entrée standard ou dans une liste de fichiers, et d'écrire les lignes qui contiennent des correspondances avec l'expression rationnelle sur la sortie standard.
> Par exemple, grep permet de chercher les lignes contenant le nom Durand dans un fichier de contacts téléphoniques :
> ```
> $ grep Durand ListeNuméros.txt
> ```
> ne renverra que les lignes contenant Durand :
> ```
> Guénolé Durand, 0723237694
> Bernard Durand, 0966324355
> ```


### 3.1.1 Utilisation de base 

**{-à faire-}** Afficher à l'aide de la commande `grep` les lignes du fichier `dictionnaire-fr.txt` qui contiennent la chaîne `étudier`.

**{-à faire-}** À l'aide des commandes `grep`, `wc` et du tube `|`, compter le nombre de mots du fichier `dictionnaire-fr.txt` qui contiennent la chaîne `étudier`.

**{-à faire-}** De la même manière, compter le nombre de mots qui contiennent la chaîne `x`.

**{+remarque+}** `grep` est sensible à la casse: les chaînes `x` et `X` sont bien différentes.
 
**{-à faire-}** Combien de mots du dictionnaire contiennent la chaîne `X` (noter la casse) ? Si vous affichez ces mots, certains devraient vous être familiers.

**{+remarque+}** L'option `-i` rend la commande `grep` insensible à la casse.

**{-à faire-}** Combien de mots du dictionnaire contiennent les chaînes `x` ou `X` ?

### 3.1.2 Chaînes en début de ligne

Si on souhaite filtrer le contenu d'un fichier en sélectionnant les lignes dans lesquelles la chaîne cherchée apparaît seulement en début de ligne, on utilise le symbole `^` qui représente le début de ligne dans le motif cherché.

**{-à faire-}** Comparez la sortie des deux commandes suivantes :
- `grep parlera dictionnaire-fr.txt`
- `grep ^parlera dictionnaire-fr.txt`

Quel est l'effet du symbole `^` dans le deuxième motif ?

**{-à faire-}** Affichez les lignes du dictionnaire qui **contiennent** la chaîne `inconstitution`.

**{-à faire-}** Affichez maintenant les lignes du dictionnaire qui **commencent** par la chaîne `inconstitution`. Les même lignes sont-elles trouvées par les deux commandes ?

### 3.1.3 Chaînes en fin de ligne

Similairement, on cherche des correspondances en fin de ligne grâce au symbole `$`.

**{-à faire-}** Comparez la sortie des deux commandes suivantes :
- `grep mangera dictionnaire-fr.txt`
- `grep mangera$ dictionnaire-fr.txt`

Quel est l'effet du symbole `$` dans le deuxième motif ?

**{-à faire-}** Affichez les lignes du dictionnaire qui **contiennent** la chaîne `aminer`.

**{-à faire-}** Affichez les lignes du dictionnaire qui **finissent** par la chaîne `aminer`. Les même lignes sont-elles trouvées par les deux commandes ?


### 3.1.4 Caractères alternatifs

Si à un endroit donné de la chaîne certains caractères sont équivalents pour notre recherche, on peut les lister entre crochets `[]`. Par exemple, si on cherche l'occurrence du mot `cote` sans vouloir distinguer les caractères `o`, `ô` entre eux, on peut utiliser le motif `c[oô]te`. Si on ne veut pas distinguer les caractères `e`, `é`, `ê`, `è` entre eux, on peut indiquer un motif du type `cot[eéêè]`.
Si on ne veut distinguer ni de `o`, `ô` ni de `e`, `é`, `ê`, `è`, on peut utiliser `c[oô]t[eéêè]`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
1. `grep cote dictionnaire-fr.txt`
2. `grep ^cote dictionnaire-fr.txt`
3. `grep cote$ dictionnaire-fr.txt`
4. `grep ^cote$ dictionnaire-fr.txt`
5. `grep ^c[oô]te$ dictionnaire-fr.txt`
6. `grep ^cot[eéêè]$ dictionnaire-fr.txt`
7. `grep ^c[oô]t[eéêè]$ dictionnaire-fr.txt`
8. `grep ^c[oô]t[eéêè] dictionnaire-fr.txt`
9. `grep c[oô]t[eéêè] dictionnaire-fr.txt`

Si beaucoup de caractères font partie de l'ensemble, on peut indiquer des intervalles grâce au tiret `-`. Par exemple, selon les [paramètres régionaux](https://fr.wikipedia.org/wiki/Param%C3%A8tres_r%C3%A9gionaux) utilisées, l'ensemble des lettres minuscules de l'alphabet latin peut être indiqué par `[a-z]`, tandis que les lettres majuscules peuvent être indiquées par `[A-Z]` et les chiffres par `[0-9]`. 

**{+remarque+}** Beaucoup d'autres classes de caractères peuvent être indiquées dans le motif de la commande `grep`. Voir la section *Character Classes and Bracket Expressions* dans la page du manuel de `grep`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
1. `grep ^b[aeiouy]ll[ayeiou]t$ dictionnaire-fr.txt`
2. `grep ^b[a-z]ll[a-z]t$ dictionnaire-fr.txt`
3. `grep ^b[a-m]ll[a-m]t$ dictionnaire-fr.txt`
4. `grep ^b[a-m]ll[n-z]t$ dictionnaire-fr.txt`
5. `grep ^b[n-z]ll[a-m]t$ dictionnaire-fr.txt`
6. `grep ^b[n-z]ll[n-z]t$ dictionnaire-fr.txt`

**{-à faire-}** Compter le nombre de mots du dictionnaire qui commencent par une voyelle.

**{-à faire-}** Compter le nombre de mots du dictionnaire qui commencent par une consonne.

**{-à faire-}** Est-ce que la somme des nombres de mots trouvés dans les deux exercices précédents correspond bien au nombre total de lignes du dictionnaire ? Vous avez peut-être oublié quelques lettres (ou alors accents, casse, ...) ?

Quand l'ensemble de caractères équivalents correspond à tous les caractères, on utilise le [métacaractère](https://fr.wikipedia.org/wiki/M%C3%A9tacaract%C3%A8re) `.`, qui représente donc un caractère quelconque dans notre motif.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
- `grep ^b[a-z]ll[a-z]t$ dictionnaire-fr.txt`
- `grep ^b.ll.t$ dictionnaire-fr.txt`

**{-à faire-}** Les deux commandes suivantes donnent presque le même résultat. Quel mot du dictionnaire est la cause de la différence ?
- `grep ^.lle dictionnaire-fr.txt | wc -l`
- `grep ^[a-z]lle dictionnaire-fr.txt | wc -l`

**{-à faire-}** Exécuter les commandes suivantes. Comment justifiez-vous leur sortie ?
1. `grep ^.$ dictionnaire-fr.txt`
2. `grep ^..$ dictionnaire-fr.txt`
3. `grep ^...$ dictionnaire-fr.txt`

**{-à faire-}** Quel motif vous permet de lister tous les mots de `dictionnaire-fr.txt` qui commencent par `a` et finissent par `l` et sont formés exactement par 5 caractères ?

**{-à faire-}** Afficher les mots qui contiennent au moins 25 caractères.

**{-à faire-}** Comparer la sortie de la commande précédente avec la sortie de `grep -E ".{25}"  dictionnaire-fr.txt`. Qu'en déduisez-vous ?

### 3.1.4 Répétitions de caractères

Si un astérisque `*` est présent dans le motif, le caractère qui le précède peut apparaître un nombre arbitraire de fois dans la ligne du fichier. Par exemple, le motif `^a*b$` correspond aux chaînes `b`, `ab`, `aab`, `aaab`, `aaaab`, etc.

**{+remarque+}** L'opérateur `*` étant un caractère spécial de [shell unix](https://fr.wikipedia.org/wiki/Shell_Unix), tout motif le contenant doit être indiqué entre guillemets ou apostrophes, par exemple `grep "^a*b$" dictionnaire-fr.txt`.

**{-à faire-}** Exécuter les commandes suivantes. Comment justifiez-vous leur sortie ?
1. `grep -i "^[er]*$" dictionnaire-fr.txt`
2. `grep -i "^[aàl]*$" dictionnaire-fr.txt`
3. `grep -i "^[aàâäeéèêëiîïoôöuùûüy]*$" dictionnaire-fr.txt`

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
1. `grep "^au..eur$" dictionnaire-fr.txt`
2. `grep "^au...eur$" dictionnaire-fr.txt`
3. `grep "^au..*eur$" dictionnaire-fr.txt`
4. `grep "^au...*eur$" dictionnaire-fr.txt`

**{-à faire-}** Trouver les mots du dictionnaire qui contiennent au moins deux `x`.

**{-à faire-}** Trouver les mots du dictionnaire qui contiennent au moins trois `z`.

**{-à faire-}** Trouver les mots du dictionnaire qui contiennent au moins cinq `a`.

**{-à faire-}** Trouver tous les mots du dictionnaire qui commencent **et** finissent par `ion`.

### 3.1.7 Chercher plusieurs motifs alternatifs

On peut indiquer deux ou plusieurs motifs alternatifs à chercher grâce au caractère '|'. Par exemple, si on veut trouver toutes les lignes de `dictionnaire-fr.txt` qui contiennent `jour` ou `soir` on peut utiliser le motif `jour|soir`. Le symbole `|` est donc l'équivalent conceptuel de l'opérateur booléen `or` de Python, mais dans le cadre de la recherche de motifs.

**{+remarque+}** Pour utiliser aisément l'opérateur `|` avec `grep`, il faut utiliser l'option `-E`. Voir la page du manuel de `grep` pour plus d'informations.

**{+remarque+}** Le symbole `|` étant un caractère spécial de shell unix (il représente aussi le *tube* vu précédemment) , tout motif de `grep` le contenant doit être indiqué entre guillemets ou apostrophes comme pour l'opérateur `*`.

**{+remarque+}**  L'opérateur de motif `|` est bien différent du tube `|`, même s'il est indiqué par le même caractère. Le contexte dans lequel ce caractère se trouve nous dit comment l’interpréter.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
1. `grep -E "^jour$|^soir$" dictionnaire-fr.txt`
2. `grep -E "jour$|^soir$" dictionnaire-fr.txt`
3. `grep -E "^jour$|soir$" dictionnaire-fr.txt`
4. `grep -E "jour$|soir$" dictionnaire-fr.txt`
5. `grep -E "jour|soir$" dictionnaire-fr.txt`
6. `grep -E "jour$|soir" dictionnaire-fr.txt`
7. `grep -E "^jour|^soir" dictionnaire-fr.txt`
8. `grep -E "jour|^soir" dictionnaire-fr.txt`
9. `grep -E "^jour|soir" dictionnaire-fr.txt`
10. `grep -E "jour|soir" dictionnaire-fr.txt`

**{-à faire-}** Concevoir un motif qui permet de lister tous les mots du dictionnaire qui contiennent `bonjour` ou `bonsoir`, et vérifier au terminal avec `grep`.

**{-à faire-}** De la même manière, trouver tous les mots du dictionnaire qui commencent par `être` ou `avoir`.

**{-à faire-}** Trouver tous les mots du dictionnaire qui commencent **ou** finissent par `ion`.

### 3.1.5 Inverser la recherche

L'option `-v` de `grep` permet de sélectionner les lignes du fichier qui **ne correspondent pas** au motif. Cela correspond donc conceptuellement à l'opérateur booléen `not` de Python.

**{-à faire-}** Afficher et compter le nombre de mots du dictionnaire qui ne contiennent pas de voyelle.

**{-à faire-}** Afficher et compter le nombre de mots du dictionnaire qui ne contiennent pas de consonne.

### 3.1.6 [Intersection](https://fr.wikipedia.org/wiki/Intersection_(math%C3%A9matiques)) de plusieurs recherches 

Une manière simple de sélectionner les lignes qui contiennent à la fois deux ou plusieurs motifs (c'est à dire d'obtenir le même effet que le `and` logique de Python) est d'enchaîner plusieurs commandes `grep` à l'aide du tube `|`.

**{-à faire-}** Exécuter les commandes suivantes, et ensuite comparer et justifier leur sortie.
- `grep pre dictionnaire-fr.txt | grep ste`
- `cat dictionnaire-fr.txt | grep pre | grep ste`

**{-à faire-}** Exécuter les commandes suivantes et justifier leur sortie :
- `cat dictionnaire-fr.txt | grep do | grep re | grep mi`


**{-à faire-}** Les commandes suivantes peuvent être simplifiées. Comment ?
- `cat dictionnaire-fr.txt | grep voir | grep -v voir.`
- `cat dictionnaire-fr.txt | grep bonne | grep -v .bonne`

### 3.1.7 Chercher dans plusieurs fichiers

L'option `-R` (pour "récursive") de `grep` permet aussi de réaliser des recherches de motifs dans des répertoires ainsi que tous leurs sous-répertoires de manière récursive. Dans ce cas, chaque correspondance de motif est précédée par le nom du fichier dans lequel la correspondance se trouve.

**{+remarque+}** Le dossier courant est indiqué par `.` ou `./`. Pour lancer une recherche récursive dans le dossier courant, on peut utiliser les commandes suivantes :
- `grep -R motif .`
- `grep -R motif ./`

Comme pour le caractère '|', la signification du `.` dépend du contexte : s'il se trouve à l'intérieur du motif, sa signification est "tout caractère", tandis que s'il se trouve après, il est à interpréter comme
partie du chemin ou du nom du fichier.


**{+remarque+}** Si on donne plusieurs options à la commande `grep`, elles peuvent être spécifiées à la suite les unes des autres.
Par exemple, à la place d'utiliser `grep -i -R -E motif fichier` on peut écrire `grep -iRE motif fichier`.

**{-à faire-}** Exécuter les commandes suivantes et justifier leur sortie :
- `grep -R ^abaissa$ ./`
- `grep -R python3 /usr/bin`
- `grep -R python /usr/bin/ | grep -v python3`

**{+remarque+}** L'option `-l` permet d'afficher seulement les noms des fichiers pour lesquels au moins une ligne du contenu correspond au motif.

**{-à faire-}** Comparer les sorties des commandes suivantes aux sorties respectives des commandes de l'exercice précédent :
- `grep -Rl ^abaissa$ ./`
- `grep -Rl python3 /usr/bin`
- `grep -Rl python /usr/bin/ | grep -v python3`

**{+remarque+}** Les chaînes `~` (tilde) ou `~/` indiquent le dossier maison. Pour lancer une recherche récursive dans le dossier maison à partir du dossier courant (autre que le dossier maison), on peut utiliser une des commandes suivantes :
- `grep -R motif ~`
- `grep -R motif ~/`

**{-à faire-}** Donner la commande qui permet de trouver tous les fichiers contenants votre nom, à l'intérieur de votre dossier maison et tout sous-dossier.

### 3.1.8 Pour aller plus loin

Des motifs bien plus complexes peuvent être utilisés avec `grep`. Voir :
- [la page Wikipédia de `grep`](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re#Classe_de_caract%C3%A8res)
- `man grep`


## 3.2 La commande find

De [Wikipédia](https://fr.wikipedia.org/wiki/Find) :

> **`find`** est une commande UNIX permettant de chercher des fichiers dans un ou plusieurs répertoires selon des critères définis par l'utilisateur.
>
> Par défaut, find retourne tous les fichiers contenus dans l'arborescence du répertoire courant. find permet aussi d'exécuter une action sur chaque fichier retrouvé, ce qui en fait un outil très puissant. 
>
> find dispose de nombreuses options pour limiter les fichiers selon leurs caractéristiques : taille, date de modification, propriétaire, type, etc.
>
> La syntaxe générale est :
> ```
> find chemin [chemin2...] [options] [action]
> ```
>
> La syntaxe de find permet de faire appel aux filtres et expressions rationnelles. Voici quelques exemples d'utilisation.
>
> Impression de la liste des fichiers sous /home qui ont été modifiés dans les 7 derniers jours :
> ```
> find /home -type f -a -mtime -7 -print
> ```
>
> Recherche de la chaîne Wikipedia dans tous les fichiers terminés par .txt sous /home :
> ```
> find /home -type f -a -name '*.txt' -exec grep -H Wikipedia {} \;
> ```
>
> Suppression des fichiers sous /tmp de plus de 14 jours ou nommés core :
> ```
> find /tmp -type f -a \( -mtime +14 -o -name 'core' \) -exec rm {} \;
> ```
>
> Recherche du fichier nom.txt récursivement à partir du répertoire courant et écrit le résultat dans le fichier sortie.txt situé sur le répertoire spécifié par chemin :
> ```
> find . -name "nom.txt" -print > ./chemin/sortie.txt
> ```

### 3.2.1 Utilisation de base

Dans sont utilisation la plus simple, la commande `find` permet d'afficher les chemins et noms des objets (dossiers, fichiers, ...) présents dans un dossier donné.

**{-à faire-}** Exécuter les commandes suivantes. Comment justifiez-vous leur sortie ?
- `find ~/`
- `find ~/Pictures`

**{-à faire-}** Comparer la sortie des commandes suivantes :
- `find ~/`
- `ls ~/`

**{-à faire-}** À l'aide des commandes `find`, `wc` et du tube `|`, spécifier et exécuter la commande qui permet de compter le nombre d'objets dans votre dossier maison.

### 3.2.2 Recherche par nom

L'option `-name` permet de spécifier le nom de l'objet à trouver.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
- `find /usr/bin`
- `find /usr/bin -name python`

L'option `-name` est sensible à la casse, tandis que l'option `-iname` ne l'est pas.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
- `find /usr/local/bin -name Thonny`
- `find /usr/local/bin -iname Thonny`

### 3.2.3 Affichage des détails

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie. Quelle est la signification de l'option `-ls` ?
- `find /usr/ -name python`
- `find /usr/ -name python -ls`

**{-à faire-}** Vérifier la signification de l'option `-ls` dans la page du manuel `man find`.

### 3.2.4 Recherche par type

La commande `find` permet de spécifier le **type** d'objet à chercher, grâce à l'option `-type`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie. Quelle est la signification des types `d` et `f` ?
- `find /usr/ -name python`
- `find /usr/ -type d -name python`
- `find /usr/ -type f -name python`

**{-à faire-}** Vérifier dans la page `man find` vos intuitions pour les types `d`, `f`. Quels autres types d'objets peuvent être cherchés ?

**{-à faire-}** À l'aide des commandes `find`, `wc` et du tube `|`, spécifier et exécuter la commande qui permet de compter le nombre d'objets **de type fichier** dans votre dossier maison.

**{-à faire-}** Toujours à l'aide des commandes `find`, `wc` et du tube `|`, répéter la même opération pour les objets de type dossier à l'intérieur de `/usr/`.

**{-à faire-}** Exécuter la commande suivante : `find /usr/ -name python -ls`. En analysant sa sortie, comment est-il possible de prédire le résultat des commandes suivantes avant de les exécuter ?
- `find /usr/ -type d -name python`
- `find /usr/ -type f -name python`
- `find /usr/ -type l -name python`

### 3.2.5 Recherche de nom par motif

Les options `-name` et `-iname` permettent de spécifier aussi des **motifs**, mais en suivant des **conventions différentes par rapport aux motifs de la commande `grep`**. Les motifs à spécifier pour `find` peuvent contenir deux [métacaractères](https://fr.wikipedia.org/wiki/M%C3%A9tacaract%C3%A8re) :
- le métacaractère `?` qui représente un caractère quelconque ; 
- le métacaractère `*` qui représente zéro, un ou plusieurs caractères quelconques.

Les métacaractères `?` et `*` des motifs de `find` correspondent donc respectivement au métacaractère `.` et à l'expression `.*` des motifs de `grep`.

**{+remarque+}** Les métacaractères `?` et `*` étant des caractères spéciaux de shell unix, tout motif le contenant doit être indiqué entre guillemets ou apostrophes, comme vu pour `grep`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier la différence en analysant leur sortie :
- `find /usr/ -iname "python*"`
- `find /usr/ -iname "*python"`
- `find /usr/ -iname "python?"`
- `find /usr/ -iname "?python"`

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `find /usr/ -iname "py?"`
- `find /usr/ -iname "py??"`
- `find /usr/ -iname "py???"`
- `find /usr/ | grep -iE "/py.$"`
- `find /usr/ | grep -iE "/py..$"`
- `find /usr/ | grep -iE "/py...$"`

**{-à faire-}** Décrire la fonction de chaque caractère dans motifs de l’exercice précédent, en décrivant s'il s'agit d'un caractère spécial (métacaractère ou opérateur) ou pas.

### 3.2.6 Recherche de chemin par motif

Les options `-path` et `-ipath` permettent de spécifier des motifs relatifs aux **chemins** des objets selon les mêmes règles utilisées pour sélectionner les noms avec les motifs des options `-name` et `-iname`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `find /usr/ -iname "*java"`
- `find /usr/ -ipath "*bin*" -iname "*java"`

**{-à faire-}** À l'aide de `find`, spécifier et exécuter la commande qui permet de lister les 
fichiers dont le nom termine par `python3`, qui se trouvent à l'intérieur du dossier `/usr/` et dont le chemin contient la chaîne `/share/`.

### 3.2.7 Opérateurs booléens

Les conditions de recherche de `find` peuvent être combinées en utilisant les opérateurs booléens de conjonction `and`, disjonction `or` et négation `not` présents aussi dans Python. Ces opérateurs sont spécifiés comme options : `-and`, `-or`, `-not`. La précédence des opérateurs peut être spécifié en utilisant des parenthèses, qui doivent être protégées par des apostrophes ou guillemets comme dans le cas des motifs contenants les caractères spéciaux `?` et `*`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `find /usr/bin/ -name "py*" -type f`
- `find /usr/bin/ -name "py*" -and -type f`
- `find /usr/bin/ -name "py*" -and -not -type f`
- `find /usr/bin/ -name "py*" -not -type f`

**{-à faire-}** Quelle est la signification des commandes suivantes ? Pourquoi leur sortie diffère ?
- `find /usr/bin/ -name "py*" -and "(" -type f -or -type l ")"`
- `find /usr/bin/ -name "py*" -and -type f -or -type l`

**{-à faire-}** Lister tous les fichiers à l'intérieur de `/usr` dont le nom contient la chaîne `python` mais pas la chaîne `python3`.

**{-à faire-}** À l'aide de `find`, `wc` et du tube `|`, compter les fichiers avec extension `.jpg`, `.png` ou `.gif` à l'intérieur de votre dossier maison.

**{-à faire-}** À l'aide de `find` et de la redirection de sortie standard `>`, créer un fichier `images.txt` qui contient la liste de tous les fichiers markdown (donc avec extension `.md`) à l'intérieur de votre dossier maison.


### 3.2.8 Pour aller plus loin

Voir :
- la [page Wikipédia de `find`](https://fr.wikipedia.org/wiki/Find)
- `man find`

## 3.3 La commande sed

De [Wikipédia](https://fr.wikipedia.org/wiki/Sed_(Unix)) :
> **`sed`** (abréviation de **s**_tream_ **ed**_itor_, « éditeur de flux ») est, comme [awk](https://fr.wikipedia.org/wiki/Awk), un programme informatique permettant d'appliquer différentes transformations prédéfinies à un flux séquentiel de données textuelles. sed lit des données d'entrée ligne par ligne, modifie chaque ligne selon des règles spécifiées dans un langage propre (appelé « script sed »), puis retourne le contenu du fichier (par défaut).
>
> sed est souvent décrit comme un éditeur de texte non-interactif. Il diffère d'un éditeur conventionnel en ceci que la séquence de traitement des deux flux d'informations nécessaires (les données et les instructions) est inversée. Au lieu de prendre une par une les commandes d'édition pour les appliquer à l'intégralité du texte (qui doit alors être intégralement en mémoire), sed ne parcourt qu'une seule fois le fichier de texte, en appliquant l'ensemble des commandes d'édition à chaque ligne. Comme une seule ligne à la fois est présente en mémoire, sed peut traiter des fichiers de taille complètement arbitraire. 
>
> L'exemple suivant montre une utilisation habituelle de sed :
> ```
> sed -e 's/Ancien/Nouveau/g' nomFichierEntrée > nomFichierSortie
> ```
>
> La commande `s` signifie substitute (« substituer »). Le drapeau `g` signifie global, ce qui indique que toutes les occurrences dans chaque ligne doivent être remplacées. Après le premier caractère `/` est donnée une expression rationnelle que sed doit trouver. Après le deuxième `/` est précisée l'expression remplaçant ce qu'il a trouvé.
>
> Une application possible de la substitution est la correction d'une faute d'orthographe récurrente, ou le remplacement de toutes > les occurrences d'un sigle. Par exemple :
> ```
> sed -e 's/ficheir/fichier/g' sed.wikipedia > sed.wikipedia.correct
> sed -e 's/WP/Wikipedia/g' sed.wikipedia > sed.wikipedia.correct
> ```
>
> Il n'est pas obligatoire d'utiliser « `/` » comme délimiteur. Les caractères « `# , - .` » (liste non exhaustive) peuvent également servir pour éviter l'accumulation d'anti-slash de « protection » rendant la syntaxe peu lisible. Les commandes suivantes sont équivalentes :
> ```
> s/\/un\/chemin\/vers/\/une\/autre\/chaine/
> 
> s#/un/chemin/vers#/une/autre/chaine#


### 3.3.1 Utilisation de base

Dans son utilisation la plus commune, `sed` permet de remplacer une chaîne de caractères `c1` par une autre chaîne `c2` dans chaque ligne de l'entrée standard, en spécifiant une option de substitution `s` dans une expression : `s/c1/c2/`. Si l'expression contient des espaces, il doit être protégé par des guillemets ou apostrophes.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `echo 'Bonjour, comment ça va ?'`
- `echo 'Bonjour, comment ça va ?' | sed s/Bonjour/Bonsoir/`

**{-à faire-}** Exécuter les commandes suivantes. Comment justifiez-vous leur sortie ?
- `head dictionnaire-fr.txt | sed s/a/X/`
- `head dictionnaire-fr.txt | sed s/A/X/`
- `head dictionnaire-fr.txt | sed s/à/X/`
- `head dictionnaire-fr.txt | sed s/b/Y/`
- `head dictionnaire-fr.txt | sed s/b/XYZ/`
- `head dictionnaire-fr.txt | sed 's/ab/  /'`
- `head dictionnaire-fr.txt | sed s/ab//`
- `head dictionnaire-fr.txt | sed 's/i/C O U C O U/'`
- `head dictionnaire-fr.txt | sed s/baiss/vou/`

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt`
- `head dictionnaire-fr.txt | sed s/a/a/`

L'option `g` permet d'appliquer la substitution partout dans chaque ligne.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed s/a/X/`
- `head dictionnaire-fr.txt | sed s/a/X/g`

**{-à faire-}** À l'aide des commandes `head`, `sed`, du tube `|` et de la redirection de sortie standard `>` créer un fichier `accents.txt` qui contient les premières 10 lignes de `dictionnaire-fr.txt` où chaque occurrence de `a` est remplacée par `à`. 

**{-à faire-}** À l'aide des commandes `head`, `sed`, du tube `|` et de la redirection de sortie standard `>` créer un fichier `pas_de_a.txt` qui contient les premières 10 lignes de `dictionnaire-fr.txt` où chaque occurrence de `a` a été supprimée.

### 3.3.2 Motifs

`sed` permet de spécifier des motifs pour l'identification des chaînes de caractères qui doivent être remplacées. Ces motifs suivent les mêmes règles vues pour les motifs de la commande `grep` :
- `^` spécifie le début de ligne,
- `$` spécifie la fin de ligne,
- les caractères entre `[` et `]` spécifient des ensembles de caractères, 
- `.` correspond à un caractère quelconque,
- `*` est l'opérateur de répétition de caractère.

Comme pour les commandes précédentes, en cas d'utilisation de caractères spéciaux comme `*`, `>`, `<`, `|` ou espaces il faut protéger l'expression par guillemets ou apostrophes.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed s/a/X/g`
- `head dictionnaire-fr.txt | sed s/^a/X/g`
- `head dictionnaire-fr.txt | sed s/a$/X/g`

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed s/a/X/g`
- `head dictionnaire-fr.txt | sed s/[aeiouy]/X/g`
- `head dictionnaire-fr.txt | sed s/[a-h]/X/g`
- `head dictionnaire-fr.txt | sed s/[a-z]/X/g`

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed s/a/a/g`
- `head dictionnaire-fr.txt | sed s/a$/a/g`
- `head dictionnaire-fr.txt | sed s/a/a$/g`
- `head dictionnaire-fr.txt | sed s/a$/a$/g`
- `head dictionnaire-fr.txt | sed s/^a/^a/g`
- `head dictionnaire-fr.txt | sed s/a/^a/g`
- `head dictionnaire-fr.txt | sed s/^a$/^a$/g`

**{-à faire-}** Exécuter les commandes suivantes. Comment justifiez-vous leur sortie ?
- `head dictionnaire-fr.txt | sed 's/^/début de ligne -> /g'`
- `head dictionnaire-fr.txt | sed 's/$/ <- fin de ligne/g'`

**{-à faire-}** Deviner la commande qui donne la sortie suivante en utilisant `dictionnaire-fr.txt` :
```
blanc-mander
démander
entre-mander
darde-mander
mander
mandera
manderai
manderaient
manderais
manderait
manderas
manderez
manderiez
manderions
manderons
manderont
m'entre-mander
remander
s'entre-mander
t'entre-mander
```
<!-- Spoiler alert !!! Solution ! A ne pas regarder avant d'avoir essayé !

```
 egrep "^manger|manger$" dictionnaire-fr.txt | sed 's/g/d/g'
```
-->

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed 's/a.a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a..a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a...a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a....a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a.....a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a......a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a.......a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a.*a/XYZ/g'`
- `head dictionnaire-fr.txt | sed 's/a*a/XYZ/g'`

**{+remarque+}** Les occurrences des séquences `\n`, `\t` dans les motifs sont interprétées 
respectivement comme « [fin de ligne](https://fr.wikipedia.org/wiki/Fin_de_ligne) » et « [tabulation](https://fr.wikipedia.org/wiki/Touche_de_tabulation#Caract%C3%A8res_de_tabulation) » comme en Python.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt`
- `head dictionnaire-fr.txt | sed 's/$/\n/g'`
- `head dictionnaire-fr.txt | sed 's/$/\n\n/g'`
- `head dictionnaire-fr.txt | sed 's/^/\t/g'`

**{-à faire-}** Exécuter les commandes suivantes et en justifier la sortie :
- `head dictionnaire-fr.txt | nl` 
- `head dictionnaire-fr.txt | nl | sed 's/ /!/g'`
- `head dictionnaire-fr.txt | nl | sed 's/\t/!/g'`


### 3.3.3 Enchaînement de substitutions

Plusieurs expressions de substitution `s/motif1/chaîne1/`, `s/motif2/chaîne2/`, `s/motif3/chaîne3/` etc. peuvent être enchaînées à l'aide du caractère `;`, en donnant `'s/motif1/chaîne1/ ; s/motif2/chaîne2/ ; s/motif3/chaîne3/'`.

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt`
- `head dictionnaire-fr.txt | sed 's/a/X/g'`
- `head dictionnaire-fr.txt | sed 's/a/X/g ; s/b/Y/g'`
- `head dictionnaire-fr.txt | sed 's/a/X/g ; s/b/Y/g ; s/i/Z/g'`

**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt`
- `head dictionnaire-fr.txt | sed 's/a/X/g'`
- `head dictionnaire-fr.txt | sed 's/a/X/g ; s/X/Y/g'`
- `head dictionnaire-fr.txt | sed 's/a/X/g ; s/X/Y/g ; s/Y/Z/g'`


**{-à faire-}** Exécuter les commandes suivantes et en justifier les similarités et les différences en analysant leur sortie :
- `head dictionnaire-fr.txt | sed 's/a/a\t/g'`
- `head dictionnaire-fr.txt | sed 's/$/\t!/g'`
- `head dictionnaire-fr.txt | sed 's/a/a\t/g ; s/$/\t!/g'`

**{-à faire-}** À l'aidé de la commande `echo` et des opérateurs de redirection de sortie standard `>` et `>>`, créer un fichier nommé `table_de_10_mots.md` avec ce contenu :
```
| Ligne | Mot |
| --- | --- |
```

**{-à faire-}** À l'aidé des commandes `head`, `nl`, `sed`, du tube `|` et de l'opérateur de redirection de sortie standard `>>`, concevoir et exécuter la commande qui permet d'**ajouter** au fichier `table_de_10_mots.md` les lignes suivantes :
```
|     1|a|
|     2|à|
|     3|AAAI|
|     4|abaissa|
|     5|abaissable|
|     6|abaissables|
|     7|abaissai|
|     8|abaissaient|
|     9|abaissais|
|    10|abaissait|
```
<!-- Spoiler alert !!! Solution ! A ne pas regarder avant d'avoir essayé !

```
echo '| Ligne | Mot |' > table_de_10_mots.md
echo '| --- | --- |' >> table_de_10_mots.md
head dictionnaire-fr.txt | nl | sed 's/^/|/ ; s/\t/|/g ; s/$/|/' >> table_de_10_mots.md

```
-->

**{-à faire-}** Vérifier que la commande `cat table_de_10_mots.md` donne la sortie suivante :
```
| Ligne | Mot |
| --- | --- |
|     1|a|
|     2|à|
|     3|AAAI|
|     4|abaissa|
|     5|abaissable|
|     6|abaissables|
|     7|abaissai|
|     8|abaissaient|
|     9|abaissais|
|    10|abaissait|
```

**{-à faire-}** À l'aide de la commande `pandoc` vue [ici](../3-markdown/Readme.md), convertir le fichier `table_de_10_mots.md` en format HTML dans `table_de_10_mots.html` et afficher le contenu de ce dernier à l'aide d'un navigateur.

**{-à faire-}** Répéter les opérations précédentes pour créer un fichier `table_de_1000_mots.html` qui contient 1000 mots de `dictionnaire-fr.txt` **choisies au hasard** mais numérotées selon le numéro de ligne d'occurrence dans `dictionnaire-fr.txt` et triées par ordre alphabétique. (Suggestion : la commande `shuf` et l'option `-n` de la commande `sort` pourraient vous aider...)

<!-- Spoiler alert !!! Solution ! A ne pas regarder avant d'avoir essayé !

```
echo '| Ligne | Mot |' > table_de_1000_mots.md
echo '| --- | --- |' >> table_de_1000_mots.md
cat dictionnaire-fr.txt | nl | shuf | head -n 1000 | sort -n | sed 's/^/|/ ; s/\t/|/g ; s/$/|/' >> table_de_1000_mots.md
pandoc -o table_de_1000_mots.html table_de_1000_mots.md
firefox table_de_1000_mots.html
```
-->

### 3.3.4 Pour aller plus loin

Voir :
- la [page Wikipédia de `sed`](https://fr.wikipedia.org/wiki/Sed_(Unix))
- le [tutoriel Ubuntu-fr de `sed`](https://doc.ubuntu-fr.org/sed)
- `man sed`


# 4. Pour aller plus loin

[Cette page](https://doc.ubuntu-fr.org/tutoriel/console_commandes_de_base) contient une liste des commandes les plus utilisées. 


