> # Utilisation d'Unix — compléments
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

# 1. Redirections des entrées/sorties

Les processus Unix possèdent des fichiers d'entrées standard, de sortie standard et d'erreur standard indépendants. Le mécanisme de redirection permet de rediriger les entrées et sorties respectivement depuis ou vers des fichiers.
	
Cette partie va illustrer ce mécanisme.

## → Redirection de la sortie standard

**{-à faire-}** exécutez les commandes suivantes
~~~
mkdir sc-redirections
cd sc-redirections
pwd
ls -la
du /usr/lib
~~~


**{-à faire-}** Dans votre éditeur de texte préféré créez un fichier nommé `compte_rendu_complements.md`, collez et complétez le texte suivant:

~~~
# Quelques commandes

|----------------------|----------------------------------------------------------------------|
| Commande             | Effet                                                                |
|----------------------|----------------------------------------------------------------------|
|mkdir sc-redirections |                                                                      | 
|cd sc-edirections     |                                                                      |
|pwd                   |                                                                      |
|ls -la                |                                                                      |
|du /usr/lib           |                                                                      |
|----------------------|----------------------------------------------------------------------|


# Contenu initial du dossier courant

~~~

**{-à faire-}** Précisez en dessous du tableau quel est le contenu précis du dossier courant. Enregistrez le fichier `compte_rendu_complements.md`.

**{+remarque+}** Dans toute la suite, on va ajouter de nouvelles choses à ce fichier. Pensez à l'enregistrer régulièrement après chaque modification.


**{-à faire-}** Executez maintenant les commandes suivantes :
~~~
du /usr/lib > texte-produit.txt
ls -la
~~~

**{-à faire-}** Constatez qu'aucun affichage ne se produit, puis ajoutez dans le fichier nommé `compte_rendu_complements.md` le texte suivant.

~~~
# Contenu du dossier courant après l'exécution de la commande `du` avec une redirection de la sortie 
~~~

**{-à faire-}** Puis visualisez avec votre éditeur de texte préféré le contenu du fichier `texte-produit.txt`

Je précise que  `>` permet de rediriger la sortie standard vers le fichier dont le nom est précisé ensuite. Attention, si le fichier existe son contenu est écrasé par la sortie crée par la commande qui précéde le `>`

Utilisée sans paramètre, la commande `cat` permet d'envoyer sur la sortie standard le contenu d'un fichier.

**{-à faire-}** Utilisez la commande `cat` pour afficher sur la sortie standard le contenu du fichier `texte-produit.txt`

La commande `cat` peut être utilisée sans paramètre. Le fichier affiché est alors l'entrée standard. On a moyen de signaler au système que l'entrée standard est terminée en tapant en début de ligne la combinaison <kbd>ctrl</kbd>+<kbd>d</kbd>. Si on teste la commande `cat` sans paramètre, la console va se comporter comme un perroquet, qui va répeter tout ce qu'on y tape jusqu'à ce que l'utilisateur tape la combinaison <kbd>ctrl</kbd>+<kbd>d</kbd>.

**{-à faire-}** Faites-le.

On peut utiliser la commande  `cat` pour réaliser un court fichier, en utilisant une redirection. 

**{-à faire-}** Réalisez le fichier `proverbe-shadok.txt` en utilisant la commande `cat > proverbe-shadok.txt` et en saisissant le texte suivant :
~~~
Il vaut mieux pomper même s’il ne se passe rien que de risquer qu’il se passe quelque chose de pire en ne pompant pas.
~~~
(N'oubliez pas  <kbd>ctrl</kbd>+<kbd>d</kbd> à la fin)

**{-à faire-}** En utilisant `ls -l`,  vérifiez que le fichier `proverbe-shadok.txt` existe bien. Vérifiez son contenu avec votre éditeur de texte préféré.

La commande `wc` (vraisemblablement de l'anglais <span lang="en">word count</span>) permet de faire des statistiques sur les fichiers, elle permet – entre autres – de compter le nombre de lignes d'un fichier. Il faut pour cela utiliser l'option `-l`  et préciser le nom du fichier.

**{-à faire-}** Combien de lignes contient le fichier `texte-produit.txt`. Copiez le texte ci-dessous à la fin de `compte_rendu_complements.md` et complétez ce qui doit l'être.

~~~
# Utilisation de `wc`

Le nombre de ligne du fichier `compte_rendu_complements.md` est ...
~~~

## → Redirection de la sortie standard à la suite d'un fichier (<span lang="en">append</span>).

**{-à faire-}** Exécutez les commandes suivantes, notez bien que l'avant dernière instruction utilise un double `>`.
En regardant attentivement le résultat de ces instructions et le titre du paragraphe, proposez une explication du rôle de `>>` 

~~~
# l'ajout en fin de fichier

|---------------------------------|----------------------------------------------------------------------|
| Commande                        | Effet                                                                |
|---------------------------------|----------------------------------------------------------------------|
|cat proverbe-shadok.txt > a.txt  |                                                                      | 
|cat proverbe-shadok.txt > a.txt  |                                                                      |
|cat a.txt                        |                                                                      |
|cat proverbe-shadok.txt >  b.txt |                                                                      |
|cat proverbe-shadok.txt >> b.txt |                                                                      |
|cat b.txt                        |                                                                      |
|---------------------------------|----------------------------------------------------------------------|

L'opérateur de redirection `>>` sert à ....
~~~

**{-à faire-}** Copiez et collez le texte précédent dans `compte_rendu_complements.md` et complétez le tableau et la phrase qui est au dessous.


## → Redirection de la sortie erreur standard
Sous linux, les processus disposent aussi d'une sortie erreur standard.
Lorsque tout se passe bien, produit ses affichages sur la sortie standard, mais quand cela se passe mal, elle produit ses affichages vers la sortie erreur standard. Toutefois à premiere vue, on ne s'en aperrçoit pas car tout est affiché sans distinction dans le terminal. 

Illustrons cela à l'aide de la commande `find`. Elle permet de chercher des fichiers dans le système de fichier. On reviendra sur cette commande plus longuement plus tard. l'option maxdepth permet de limiter la profondeur de recherche dans l'arbre du système de fichier. la commande 
~~~
find / -maxdepth 3
~~~
permet de trouver tous les éléments du système de fichiers qui sont à profondeur 3 à partir de la racine. 

Regardez attentivement le resultat. 
Vous devez voir deux types de lignes, certaines sont du genre `find:... Permission non accordée`. d'autre correspondent à des noms absolus de fichier ou de dossier. Les premieres sont imprimées sur la sortie erreur les secondes sur la sortie standard. Pour s'en convaincre, il suffit de faire une redirection de la sortie. 

**{-à faire-}** Exécutez la commande : 
~~~
find / -maxdepth 3 > trouve.txt
~~~
Que constatez vous ? 

**{-à faire-}** Exécutez la commande : 
~~~
find / -maxdepth 3 > trouve.txt 2> erreur.txt
~~~
Que constatez vous ? 




## → Redirection de l'entrée standard
Lorsqu'on utilise la commande `wc -l` sans paramètre en plus de l'option. c'est le nombre de lignes de l'entrée standard qui est compté. 

**{-à faire-}** Testez cette commande et utilisez le texte ci-dessous pour l'entrée standard.

<pre>
a
b
c
d
<kbd>ctrl</kbd>+<kbd>d</kbd>
</pre>

On peut demander au système de prendre le contenu d'un fichier et d'utiliser ce contenu pour la commande `wc -l`. 

**{-à faire-}** Testez la commande suivante `wc -l < proverbe-shadok.txt`

**{-à faire-}** Complétez le compte rendu avec le texte suivant :
~~~
# Redirection de l'entrée standard
l'opérateur `<` permet de faire une redirection de l'entrée standard. lors de l'exécution de la commande située à gauche, au lieu de lire ses entrées sur l'entrée standard, les entrées vont être lues dans le fichier dont le nom est précisé à droite.
~~~

**{-à faire-}** Prévoyez l'effet de la commande suivante `cat < b.txt`. 
	
## → Le tube

L'opérateur `|` sert à enchainer des commandes. Dans ce contexte, cette barre verticale est appelée <q>tube</q> (en anglais <span lang="en">pipe</span>).
~~~
commande_1 | commande_2
~~~
possède a peu près la signification suivante :
~~~
commande_1 > fichier-temporaire
commande_2 < fichier-temporaire
~~~
sauf que le fichier l'environnement n'est pas pollué par la création du fichier `fichier-temporaire`
On peut enchainer ainsi plusieurs commandes:
~~~
commande_1 | commande_2 | commande_3 | ... | commande_n
~~~
possède à peu près la signification suivante:
~~~
commande_1 > resultat_1
commande_2 < resultat_1 > resultat_2
commande_3 < resultat_2 > resultat_3
...
commande_n < resultat_nmoins1
~~~

**{-à faire-}** Utilisez un tube pour connaitre le nombre de lignes produites par l'exécution de 
`du -h /usr/lib`


**{-à faire-}** Téléchargez le fichier [materiel_complements.zip](./materiel_complements.zip)
On peut aussi le faire en ligne de commande : `wget https://gitlab-fil.univ-lille.fr/ls1-odi/portail/-/raw/master/1-fs+terminal/materiel_complements.zip`

**{-à faire-}** Décompressez le dans le dossier courant.
On peut aussi le faire en ligne de commande : `unzip materiel_complements.zip`

le fichier `melange` est une commande qui a pour effet de lire l'entrée standard et de recopier les lignes lues sur la sortie standard dans un ordre aléatoire

le fichier `binomise` est une commande qui a pour effet de lire l'entrée standard et de joindre les lignes par groupe de deux. 

**{-à faire-}** Testez les commandes suivantes
~~~
cat liste_eleve.txt
cat liste_eleve.txt | ./melange
cat liste_eleve.txt | ./melange
cat liste_eleve.txt | ./binomise
cat liste_eleve.txt | ./binomise
cat liste_eleve.txt | ./melange | ./binomise
cat liste_eleve.txt | ./melange | ./binomise
~~~
**{+remarque+}** Chaque commande sauf la première est répétée deux fois.
