> # Le système de fichiers et le terminal
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr) 


## → Objectifs

Ce document vise à vous présenter le système de fichiers Linux utilisé sur les machines des salles de <abbr title="travaux pratiques">tp</abbr>. 

On présente dans ce document l'organisation du système de fichiers, des notions importantes comme chemin relatif et chemin absolu que vous retrouverez dans d'autres cours. 

Bien que la présentation soit particulièrement orientée sur la configuration des salles de <abbr title="travaux pratiques">tp</abbr>, la majorité des concepts peuvent se retrouver sur vos machines personnelles même si celles-ci n'utilisent pas le même système d'exploitation.


# 1. Le terminal, la console

## → Introduction

La plupart des systèmes sont pourvus d'une interface graphique dont l'intention prétendue est de rendre l'utilisation plus simple et plus intuitive. Toutefois, il peut arriver que les interfaces graphiques cachent une partie de la difficulté et entretiennent les utilisateurs dans un état de dépendance. Chaque changement d'interface nécessite une période de réapprentissage qui diminue la productivité pendant cette période. D'autre part certaines tâches répétitives peuvent être plus difficiles à automatiser en utilisant l'interface graphique. Enfin, ici il convient de faire de vous des utilisateurs avertis. Il a donc été décidé de vous présenter comment utiliser le terminal.

## → Le terminal

Il s'agit d'un programme interactif, l'opérateur tape des commandes. Ces commandes sont analysées puis exécutées par le programme qui modifie l'état du système. Cette modification peut, par exemple, consister à l'affichage d'un résultat ou des effets sur les fichiers ou des effets sur les processus en cours. 

En français, on appelle ce programme, terminal, console, ou interprète de commande. En anglais, on l'appelle aussi <span lang="en">shell</span>. La coquille est ce qui fait l'interface entre l'interieur, -le système- et l'exterieur, -l'utilisateur-. 

**{+remarque+}** Il existe plusieurs variantes de <span lang="en">shell</span> Linux. Mais pour l'instant cela importe peu.

Lançons le terminal et observons.

![Fenêtre Shell](./img/fenetre-shell.png)

L'ordinateur affiche un message qui donne quelques informations sur l'état du système. Ce message s'appelle le *message d'invite* ou simplement *l'invite* (en anglais <span lang="en">*prompt*</span>). 

Généralement, on y trouve les réponses aux questions *qui* et *où*.
On va détailler plus loin sur le message d'invite.

**{+remarque+}** Ce message est personnalisable.

En plus des informations affichées, cela indique que le système est prêt à recevoir une commande.

La syntaxe d'une commande est extrêmement simple, on écrit la commande suivie des paramètres éventuels séparés par des espaces.

**{+remarque+}** Le choix du séparateur a pour conséquence qu'il est déconseillé d'utiliser l'espace dans les noms de fichiers. Ce n'est pas interdit, mais cela peut rendre plus complexe l'écriture de certaines commandes.

![Pourquoi faire simple](./img/pourquoi-faire-simple.png)

La commande `man` permet d'obtenir les documentations du systèmes et des commandes. 

**{-à faire-}** Testez cette commande dans le terminal.

`man`

**{-à faire-}** Testez la commande
`man man`

Pour faire défiler le texte utilisez les flèches <kbd>haut</kbd> et <kbd>bas</kbd> du clavier. tapez <kbd>q</kbd> pour quitter.

**{-à faire-}**
À quoi sert la commande `ls` ?


**{-à faire-}** 
À quoi sert la commande `pwd` ?

Le manuel est une description assez complète des commandes et de leur options. On s'en sert plutôt comme référence lorsqu'on recherche des fonctionnalités assez précises. Mais il ne serait pas très commode de découvrir le terminal en faisant la lecture <span class="locution latine" title="en entier">in extenso</span> de la totalité des pages du manuel. Aussi dans la suite, on va présenter quelques commandes usuelles en expliquant leur effet et en ne détaillant que quelques options les plus communes. Si vous souhaitez davantage d'information s'il vous plaît <abbr lang="en" title="read the f... manual">rtfm</abbr>[^rtfm]. 

[^rtfm]: <abbr lang="en" title="read the f... manual">rtfm</abbr> est un acronyme en langue anglaise qui signifie – dans sa version présentable – <span lang="en">read the fine manual</span>. Dans la version moins présentable le mot <span lang="en">fine</span> est remplacé par un autre mot...

# 2. Quelques commandes

Voici une liste de quelques commandes :

- `date`
- `python3` (pour quitter l'interprète python tapez `quit()`
- `thonny-l1test` 
- `lowriter` (pour retrouver le prompt quittez libreoffice writer)
- `localc` (idem)
- `history`

**{-à faire-}** 
testez ces commandes

On va présenter d'autres commandes après la description du système de fichiers.

**{+remarque+}**
La complétion automatique permet de d'économiser l'usure des empreintes digitales, c'est-àdire de moins presser de touche. par exemple pour taper la commande `thonny-l1test`, il suffit de taper <kbd>t</kbd><kbd>h</kbd><kbd>o</kbd><kbd>Tab</kbd>,  la machine affiche alors les deux complétions possible de ce début de commande, qui sont `thonny` et `thonny-l1test` et complète alors avec `nny` en tapant ensuite <kbd>-</kbd><kbd>Tab</kbd> la commande est complétée.

**{-à faire-}**
Vérifiez le.

**{-à faire-}**
Lancez libre-office Writer depuis la console en utilisant la complétion.


**{+remarque+}**
La commande `history` permet de connaitre les dernières commandes utilisées et éventuellement de les réutiliser.
l'affichage obtenu est composé de la liste des dernières commandes chaque commande est précédée d'un numéro. en tapant <kbd>!</kbd> on peut relance la commande correspondante.

# 3. Le système de fichiers

## → Dossiers
On appelle dossier ou répertoire un élément du système de fichiers qui peut
contenir d'autres dossiers ou des fichiers. un dossier ou répertoire est le mot français  utilisé pour traduire <span lang="en">directory</span> ou <span lang="en">folder</span>.

La commande `du` (<span lang="en">disk usage</span>) permet de connaitre la taille utilisée par un dossier. En précisant l'option `-h` (comme <span lang="en">human readable</span>)  on permet d'avoir un résultat présentant sous une forme plus agréable pour les humains (le programme choisit une unité adaptée). En précisant l'option `-s` (comme <span lang="en">summarize</span>), on demande de nous afficher que la taille totale du dossier.

**{-à faire-}** Exécutez la commande
`du -hs /usr/lib`

**{-à faire-}** Quelle commande utiliser pour connaitre la taille du dossier ~/Téléchargements ? 

Les dossiers possèdent un nom et différents attributs.

## → Fichiers

Les fichiers servent à contenir les données. On peut considérer que le contenu d'un fichier est une liste (éventuellement vide) de nombres entiers. Chaque nombre entier est compris entre `0` et `255`. L'ordre des éléments dans la liste est important.

Comme les dossiers, Les fichiers possèdent un nom et différents attributs.

## → Les noms des éléments

la barre oblique `/` est interdite dans les noms de fichiers.

Evitez aussi les `:` et les `\`, les espaces, les tabulations, les retours à la ligne.

l'octet NUL est interdit dans les noms de fichiers.

La longueur maximale (pour `ext4fs`) d'un nom de fichier ou dossier est 255 octets.

Le point est autorisé une ou plusieurs fois dans les noms de fichiers.

Lorsqu'un nom de fichier ou dossier commence par un point celui-ci est considéré comme un fichier caché.

**{+remarque+}** Dans le système, il existe des fichiers cachés. Ils sont cachés car l'utilisateur n'est pas censé intervenir dessus directement. Ils sont créés par le système ou par des applications pour stocker des préférences des réglages, des historiques... Toutefois, il peut arriver dans certains cas bien particulier qu'on ait besoin d'y accéder. l'option `-a` (comme <span lang="en">all</span>) de la commande `ls` permet de voir les fichiers cachés.

**{-à faire-}** Dans un terminal, tapez la commande `ls`

**{-à faire-}** Dans un terminal, tapez la commande `ls -a`

### Extensions

usuellement, on attribue un suffixe qui commence par un point et qui est suivi de quelques caractères. On appelle cela l'extension du fichier. Cette extension peut être une indication de ce que contient le fichier. ainsi le l'extension `txt` indique que le contenu du fichier est du texte, `py` du python, etc... Dans certains systèmes, cette extension est parfois cachée. 



## → La racine
Un unique dossier sert de point de départ du système de fichiers. C'est le seul élément du système de fichiers dont le nom est la chaine vide. Tous les autres éléments du système de fichiers sont des *descendants* de la racine.

## → La maison
Les utilisateurs du système possèdent un dossier qui leur appartient. C'est dans leur dossier que les fichiers peuvent être créés et stockés.

La maison est désignée par `~` 



## → Dossier courant
Lorsque le terminal est lancé, un dossier est courant est défini, en général il s'agit de la maison de l'utilisateur.

On peut connaître le dossier courant en tapant l'instruction `pwd` qui est un acronyme de <span lang="en">print working directory</span>

**{-à faire-}** Dans un terminal, tapez la commande `pwd`

tout dossier contient deux éléments (cachés car ils commencent par un point) `.` et `..` qui désigne le dossier lui même et son père (sauf biensûr pour la racine...)



### Changer le dossier courant.
La commande `cd`  permet de changer de dossier. Le nom de la commande provient de l'anglais <span lang="en">change directory</span>


**{-à faire-}** la commande `cd`, utilisée sans paramètre permet de revenir à la maison. Faites-le.

**{-à faire-}** Tapez `cd Téléchargements`. Notez l'évolution du prompt. tapez l'instruction `pwd`.

**{-à faire-}** la commande `cd`, utilisée sans paramètre permet de revenir à la maison. Faites-le.

**{-à faire-}** Tapez la commande `cd ..`, puis la commande `pwd`, notez l'évolution du message d'invite (<span lang="en">prompt</span>)

**{-à faire-}** Faites ce qu'il faut pour <q>revenir à la maison</q>.


## → Création et suppression de dossiers
la commande `mkdir` permet de creer un dossier. Le nom de la commande provient de l'anglais <span lang="en">make directory</span>. Le nom du dossier à créer doit être passé en paramètre.

**{-à faire-}** Lancez un terminal, puis tapez la commande `mkdir toto`.
Vérifiez ensuite avec un gestionnaire de fichiers graphique que le dossier `toto` a bien été  créé.

la commande `rmdir` permet d'effacer un dossier. Il est indispensable que le dossier soit vide pour pouvoir le supprimer grâce à cette commande.

**{-à faire-}** Effacez le dossier `toto` précédemment créé. Vérifiez ensuite avec un gestionnaire de fichiers graphique que le dossier `toto` a bien été supprimé.

## → Création et suppression d'un fichier

La commande `touch` permet d'actualiser l'heure de modification d'un fichier existant. Si le fichier est inexistant, il est créé avec une taille nulle.

**{-à faire-}** Exécutez la commande `touch titi`. Vérifiez ensuite avec un gestionnaire de fichiers graphique que le fichier `titi` a bien été créé.

la commande `rm` permet d'effacer un fichier. Faîtes très attention quand vous l'utiliserez, une maladresse est vite arrivée.

**{-à faire-}** Tapez `rm titi`, confirmez si nécessaire, puis vérifiez avec un gestionnaire de fichiers graphique que le fichier `titi` a bien été supprimé.


## → Noms absolus et noms relatifs
Le nom absolu d'un fichier est le chemin obtenu en construisant la liste noms des répertoires à traverser depuis la racine inclue pour atteindre le fichier, puis en ajoutant en fin de liste le nom du fichier. Ensuite on intercale une barre oblique `/` entre chaque élément de la liste. La barre oblique signifie qu'il faut traverser le dossier. Comme le nom de la racine est la chaîne vide le chemin absolu commence par une barre oblique `/`.

**{+remarque+}** Dans d'autres systèmes le caractère qui représente la traversée d'un dossier peut être différent, par exemple `\` sous windows, ou `:` sous mac.


Un nom relatif d'un fichier est le chemin obtenu en construisant la liste des noms des répertoires à traverser depuis le dossier courant. On ajoute le nom du fichier en fin de liste. Puis on intercale une barre oblique entre chaque élément de la liste. Si le fichier est dans le dossier courant, la liste est réduite à un seul élément, le nom du fichier et il n'y a pas de barre oblique. Comme seul la racine est nommée par une chaîne vide, un nom relatif ne peut pas commencer par une barre oblique. C'est comme cela qu'on peut faire la différence entre un nom relatif et un nom absolu.

**exemples:**

~~~ 
/
├── codage
│   ├── cm
│   └── td
├── math
│   ├── cm
│   ├── et
│   └── td
├── maths-discretes
│   └── ctd
├── odi
│   └── tdm
└── prog
    ├── cm
    ├── td
    └── tdm

~~~ 

Imaginons que le dossier `cm` du dossier `prog` contienne un fichier nommé `seance1.txt`. Le nom absolu de ce fichier est `/prog/cm/seance1.txt`.
Maintenant si le dossier courant est le dossier `td` du dossier `prog` alors le nom relatif de ce même fichier est `../cm/seance1.txt`.

**{-à faire-}** Trouvez le chemin absolu d'un fichier nommé `td1.pdf` qui est situé dans le dossier `ctd` du dossier `maths-discretes`.

**{-à faire-}** Déterminez quel est le chemin relatif de ce même fichier nommé `seance1.txt` si le dossier courant est `odi/tdm` .

## → Quelques attributs des fichiers.
taille, date de création, date de modification, date du dernier accès, propriétaire, groupe, droits d'accès, etc... sont des attributs possibles des fichiers. Vous pouvez en consulter certain en utilisant l'option `-l` de la commande `ls`. 

**{-à faire-}** Faites-le ! et combinez avec l'option `-a` en tapant `ls -la`.


# 4. D'autres commandes 

- `pwd`
- `ls`
- `mkdir`
- `cd` 
- `rmdir`
- `touch`
- `tree`
  permet d'afficher de manière textuelle sous forme d'un arbre le contenu d'un dossier et des sous dossier.  On précise le chemin du dossier dont on veut produire l'arbre. à défaut c'est le dossier courant qui va être utilisé.

- `zip`
  permet de fabriquer une archive compressée au format `zip`. Son utilisation est assez simple `zip nom_archive liste_des_elements_a_ajouter`. 

**{+Attention+}** Lorsqu'on souhaite ajouter un dossier, si on ne précise pas l'option `-r` (pour récursif)  seul le dossier est ajouté dans l'archive mais pas le contenu du dossier. 
- `zipinfo`
  permet d'afficher le contenu d'une archive au format `zip`. Il suffit de préciser le nom de l'archive
- `unzip` 
  permet la décompression d'un fichier. Il faut préciser en paramètre le nom de l'archive à décompresser.
- `tar`
  permet de faire une archive au format `tar`.
- `realpath`
- `chmod`
- `hexdump`
  permet de visualiser le contenu d'un fichier sous forme hexadécimale. l'option `-C` permet un affichage *canonique*. En première colonne on trouve l'adresse (<span lang="en">offset</span>) et ensuite on trouve seize colonnes avec le contenu de chaque octet en hexadécimal et enfin seize caractères ascii correspondant lorsqu'ils sont affichables et un point à la place sinon.
  On précise en paramètre le nom du fichier à afficher.
  
  
  
- `hexedit`
  permet de visualiser et de modifier le contenu d'un fichier sous forme hexadécimale. (on reviendra sur cette commande dans une autre séance)
  
- `wget`

# 5. Résumé

Pour pouvoir retrouver ses travaux, qu'on utilise un outil graphique, ou qu'on utilise la console, il est important de connaitre comment est organisé le stockage de l'information. En première approximation, on peut considérer que le système de stockage est organisé en fichiers et en dossiers. Les dossiers peuvent contenir des fichiers et d'autres dossiers. Les fichiers peuvent être vus comme une suite finie et ordonnée d'octets. Un octet (byte en anglais) est un entier naturel compris entre 0 et 255. Les fichiers et les dossiers possèdent un nom et quelques attributs. Un unique dossier utilise le mot vide comme nom. Il s'agit de la racine. La structure du système de fichiers est un arbre. Pour obtenir le nom absolu d'un fichier on commence à la racine et on liste les dossiers qu'il faut traverser pour atteindre le fichier. à chaque fois qu'on traverse un fichier on utilise une barre oblique pour indiquer qu'on a traversé le dossier. Enfin, on place le nom du fichier.
Lorsqu'on utilise un gestionnaire de fichiers graphique, ou lorsqu'on utilise la console, on change de dossier courant. La méthode pour le faire avec le gestionnaire de fichiers est assez intuitive généralement il suffit de cliquer sur le dossier. Lorsqu'on utilise la console, on doit le faire en utilisant la commande cd (change directory) (changer de dossier)


