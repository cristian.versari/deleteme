 > # Navigateur web
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

# 1. Objectifs

Ce document vise à vous présenter les fonctionnalités de base d'un
[navigateur web](https://fr.wikipedia.org/wiki/Navigateur_web) ainsi
que les sites web principaux qui vous seront utiles pendant vos
études de licence.

# 2. Le navigateur web

Un navigateur web est une application desormais toujours présente
sur un poste informatique. 

![Navigateur web](./img/navigateur-web.png)

Elle vous permet de visualiser des documents ou en général
d'acceder à des ressources publiées sur
le [World Wide Web](https://fr.wikipedia.org/wiki/World_Wide_Web).
Vous avez normalement déjà appris 
comment démarrer
les navigateurs web présents dans les postes de travail à votre
disposition dans la partie de découverte à l'environnement graphique.

## → Les URLs

Les réssources publiées sur le World Wide Web peuvent être récupérées 
en connaissant l'[_URL_](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator) 
correspondante (de l’anglais : Uniform Resource Locator, localisateur 
uniforme de ressource).

Un exemple simple d'URL est
`https://www.fil.univ-lille.fr/portail/`.
Cette URL se compose de trois parties :
- le nom du 
  [protocole réseau](https://fr.wikipedia.org/wiki/Protocole_r%C3%A9seau) :
  [HTTPS](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure) ;
- le [nom de domaine](https://fr.wikipedia.org/wiki/Nom_de_domaine) 
  du [serveur informatique](https://fr.wikipedia.org/wiki/Serveur_informatique) 
  qui hébèrge la page web : `www.fil.univ-lille.fr` ;
- le [chemin d'accès](https://fr.wikipedia.org/wiki/Chemin_d%27acc%C3%A8s) 
  à la ressource spécifique : `/portail/`.

**{-à faire-}** Si vous êtes en train d'utiliser un navigateur web
pour visualiser ce document,
essayez d'identifier le protocole réseau et le nom de domaine du serveur
en analysant l'URL qui se trouve maintenant dans la 
[barre d'adresse](https://fr.wikipedia.org/wiki/Barre_d%27adresse)
du navigateur, typiquement située au dessus de la page web que vous
êtes en train de lire.

![Barre d'adresse du navigateur web](./img/barre-adresse.png)

## → Les fenêtres de navigation

Un navigateur web permet de visiter une ou plusieurs URLs pour récuperer 
les ressources correspondantes. Chaque ressource est normalement visualisée 
dans une [fenêtre de navigation](https://fr.wikipedia.org/wiki/Fen%C3%AAtre_(informatique))
différente.
La plupart des navigateurs modernes permet d'ouvrir **plusieurs fenêtres** de navigation
en même temps, pour acceder à plusieurs ressources simultanément.

![Plusieurs fenêtres de navigation](./img/plusieurs-fenetres.png)
 
Une fois ouvertes plusieurs fenêtres, on peut normalement passer d'une 
fenêtre de navigation à l'autre en utilisant les mêmes techniques 
et raccourcis clavier que vous avez découvert dans la partie de découverte
de l'environnement graphique.


**{-à faire-}** Si vous ne l'avez pas encore fait,
à partir du menu application ouvrez un navigateur web 
[Mozilla Firefox](https://fr.wikipedia.org/wiki/Mozilla_Firefox)
ou
[Google Chrome](https://fr.wikipedia.org/wiki/Google_Chrome).
Sinon, passez à une fenêtre quelconque de votre navigateur
et ouvrez une nouvelle fenêtre de 
navigation avec le raccourci clavier <kbd>Ctrl</kbd>+<kbd>N</kbd>.

**{-à faire-}** Dans cette nouvelle fenêtre de navigation, 
visitez l'URL `https://www.fil.univ-lille.fr/portail/` soit en modifiant
le contenu de la barre d'adresse directement avec votre clavier,
soit en copiant/collant l'URL avec la méthode de votre choix. 

**{-à faire-}** Maximisez à gauche la fenêtre de navigation de 
l'exercice précédent en utilisant le raccourci clavier 
<kbd><img src="../1-env-graphique/img/winlogo.png" alt="logo window"/></kbd> + <kbd>Gauche</kbd>
vu dans la partie d'introduction à l'environnement graphique.

**{-à faire-}** Ouvrez une nouvelle fenêtre de navigation
avec le raccourci clavier <kbd>Ctrl</kbd>+<kbd>N</kbd>,
maximisez la fenêtre à droite en utilisant le raccourci
<kbd><img src="../1-env-graphique/img/winlogo.png" alt="logo window"/></kbd> + <kbd>Droite</kbd>.

**{-à faire-}** Dans cette nouvelle fenêtre de navigation, visitez
l'URL `https://portail.fil.univ-lille.fr`.

**{-à faire-}** Sans les déplacer, visualisez côte à côte lez deux 
fenêtres que vous avez ouvert dans les exercices précédents. Vous
devriez obtenir un placement de fenêtres comme dans l'image ci-dessous :
![Deux fenêtres de navigation côte à côte](./img/fenetres-cote-a-cote.png)

**{-à faire-}** Comparez les URLs qui apparaissent dans les barres d'adresses
de vos deux fenêtres de navigation. Que notez vous ?

**{+pour aller plus loin+}** Est ce que le concept expliqué
dans cette page
`https://fr.wikipedia.org/wiki/Redirection_d%27URL`
pourrait expliquer votre découverte ?

**{-à faire-}** Fermez les fenêtres de navigation que vous avez ouvert
dans les exercices précédents.

## → Les hyperliens

Les pages web intègrent de manière naturelle les URLs à travers les
[hyperliens](https://fr.wikipedia.org/wiki/Hyperlien), qui cachent les
URLs dans des _liens cliquables._

**{-à faire-}** Lisez le document identifié par l'URL caché dans 
l'hyperlien suivant en le cliquant avec le bouton gauche de la souris :
[→cliquez⠀ici←](https://arpinux.org/discestquoi/dcq/dcq-web.pdf).

**{-à faire-}** Cliquez maintenant l'hyperlien de l'exercice précédent
en utilisant plutôt le **bouton droit** de la souris. Un
[menu contextuel](https://fr.wikipedia.org/wiki/Menu_contextuel)
apparaîtra, similaire à celui montré ci-dessous :

![Menu contextuel d'hyperlien](./img/menu-contextuel-hyperlien.png)

**{-à faire-}** Dans le menu contextuel que vous venez d'ouvrir,
sélectionnez l'entrée qui vous permet de copier
l'URL dans le [presse-papiers](https://fr.wikipedia.org/wiki/Presse-papiers_(informatique)).
Ouvrez une nouvelle fenêtre de navigation avec la touche <kbd>Ctrl</kbd>+<kbd>N</kbd>
et collez l'URL dans la barre d'adresse sans visiter la ressource.

**{-à faire-}** Identifiez dans l'URL que vous venez de coller
dans l'exercice précédent les parties qui le composent : 
le protocole réseau, le nom de domaine et le
chemin vers la ressource.

**{-à faire-}** Devinez comment modifier la partie "chemin" de 
l'exercice précédent pour trouver d'autres bandes
déssinées hébergées sur le même serveur.

**{-à faire-}** Une fois lue(s) la(les) bande(s) déssinées qui vous
intéressent, fermez la(les) fenêtre(s) de navigation que vous venez
d'ouvrir.

**{-à faire-}** Ce document contient un nombre considerable d'hyperliens
vers des pages de Wikipedia. Visitez-les et approfondissez votre culture
informatique.

## → Les onglets du navigateur web

L'ouverture de nouvelles fenêtres de navigation est pratique si le nombre
de fenêtres ouvertes en même temps reste limité. Dans le cas contraire,
la plupart des navigateurs web modernes, de façon similaire à beaucoup 
d'autres applications graphiques, permet de regrouper dans une seule 
fenêtre de navigation plusieurs pages web ouvertes, en utilisant 
le concept 
d'[onglet](https://fr.wikipedia.org/wiki/Onglet_(informatique)).
Les onglets apparaissent normalement juste au dessus de la barre d'adresse
du navigateur.

![Onglets du navigateur web](./img/onglets.png)


**{-à faire-}** Cliquez avec le bouton droit de la souris l'hyperlien
suivant :
[→cliquez⠀ici←](https://fr.wikipedia.org/wiki/Onglet_(informatique)#Navigation_par_onglets).
Dans le menu contextuel qui s'ouvre, sélectionnez l'entrée qui vous
permet d'ouvrire la page web correspondante dans un nouvel onglet.

**{-à faire-}** Une fois ouverte la page, 
visualisez l'onglet qui vient de s'ouvrir si le navigateur ne l'a pas 
déjà sélectionné pour vous.

**{-à faire-}** Une fois lu le document, fermez l'onglet que vous
venez d'ouvrir.

**{-à faire-}** Le 
[portail du département informatique de la Faculté de Sciences et Technologies de l'Université de Lille](https://portail.fil.univ-lille.fr)
contient une page web spécifique pour la L1 MATHS-INFO. Visitez le lien,
cherchez la page et visitez-la.

**{-à faire-}** Dans la page ouverte dans l'exercice précédent, trouvez 
l'hyperlien vers la sous-page qui décrit le premier 
semestre (S1) de la L1 MATHS-INFO et ouvrez-la **dans un nouvel onglet**.

**{-à faire-}** Identifiez maintenant dans cette sous-page 
les hyperliens vers les UEs du S1 de la L1 MATHS-INFO.
Ouvrez chaque hyperlien dans un nouvel onglet.

**{-à faire-}** Chaque UE contient plusieurs sous-sous-pages, nommées
_Présentation_, _Programme_, _Emploi du temps_, _Semainier_, etc.
Ouvrez chaque sous-sous-page de chaque UE dans un nouvel onglet.

**{-à faire-}** Après avoir lu les informations importantes sur les UEs
de votre licence, cliquez avec le **bouton droit** de la souris sur le premier
onglet de la fenêtre de navigation. Dans le menu contextuel qui apparaît, 
sélectionnez l'entrée qui vous permet de fermer tous les onglet à droite 
du courant.

**{-à faire-}** Cliquez avec le bouton droit de la souris sur un onglet
de votre choix, en faisant apparaître le menu contextuel correspondant.
Étudiez les differentes options proposées par ce menu.


# 3. Sites web d'intérêt

## → Les sites des départements

Le portail L1 mathématiques-informatique est une formation de la Faculté des 
Sciences et Technologies de l'Université de Lille. Sur le 
[site de la faculté](https://sciences-technologies.univ-lille.fr/)
vous trouverez de nombreuses informations, dont une rubrique sur la vie étudiante. 
Vous trouverez également des liens vers les deux départements d'enseignement 
dont dépend la L1 :

- le département [d'informatique](https://sciences-technologies.univ-lille.fr/informatique), 
  historiquement appelé FIl pour Formation en Informatique de Lille ;
- le département [de mathématiques](https://sciences-technologies.univ-lille.fr/mathematiques).

**{-à faire-}** Visitez les pages spécifiées ci-dessus.

## → Le portail pédagogique du département informatique

Les formations gérées par le département informatique et la description de leurs UE sont présentées dans le 
[portail pédagogique](https://www.fil.univ-lille.fr/portail) que vous avez visité
dans des exercices précédents.

Les années de formation, semestres puis UE sont accessibles dans des pages et sous-pages spécifiques.

**{-à faire-}** Explorez les formations disponibles sur ce portail.
