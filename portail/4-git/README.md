> #  Introduction à Git et GitLab
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

[Git](https://en.wikipedia.org/wiki/Git) est un [logiciel de gestion de versions décentralisé](https://en.wikipedia.org/wiki/Distributed_version_control). 

C'est un [logiciel libre et gratuit](https://en.wikipedia.org/wiki/Free_and_open-source_software), créé en 2005 par [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds) (auteur du [noyau Linux](https://en.wikipedia.org/wiki/Linux_kernel) qui déclare s'amuser à donner à ses créations des noms relatifs à sa personne[^1]). 

[^1]: *git (plural gits): (Britain, slang, derogatory) A silly, incompetent, stupid, annoying or childish person (usually a man).*

## 1. Quoi et pourquoi ?

Un système de contrôle de version est un ensemble d’outils permettant de :

-   mémoriser et retrouver différentes versions d’un projet ;

-   faciliter le travail collaboratif multi-plateforme.

Initialement développé par Linus Torvald pour l’évolution du noyau Linux, l’outil dominant est actuellement Git (en 2023). 

C’est :

-   un outil décentralisé ;

-   sous licence <span class="smallcaps">gnu</span> <span class="smallcaps">gpl</span> (*a.k.a.* logiciel libre open source) ;

-   disponible sous toutes les plateformes.


### Pourquoi ?

Git est incontournable pour le développement logiciel mais aussi pour celui d'autres supports (écriture articles, pages web statiques[^2], etc).

[^2]: la rédaction du cours que vous êtes en train de suivre est faite grâce à git.

Git est fait pour les fichiers textes (pour les fichiers "binaires" utilisez [git-lfs](https://git-lfs.com/) *a.k.a. large file storage*).

#### Un peu de vocabulaire

Une *version* (*i.e.* un commit) est l'état d'un projet à une étape de son cycle de vie.

Une *branche* est constituée de plusieurs versions et résume l'évolution du projet principal ou d'une de ses variantes.

Un *dépôt* est l’historique du projet constitué par toutes ses branches.

#### Utilité d'un système de gestion de version

Git permet :

-   d’enregistrer les versions du projet dans un dépôt et de retrouver une version spécifique ;

-   de travailler sur différentes branches (de les créer, de les faire évoluer et de les fusionner) ;

-   de partager les versions et principalement d'ajouter à son dépôt des versions faites par d’autres.

## 2. Comment ?

Nous allons commencer par illustrer l'utilisation de git en créant pas à pas un dépôt en local sur votre machine de travail.

**{+remarque+}** Vous devez reproduire chaque étape de cette section. Cela signifie que toute manipulation au terminal montrée dans cette section est **{-à faire-}**.

### 2.1 Utiliser git en ligne de commande

Les commandes de git sont des filtres shell du type :

~~~
% git "command".
~~~

Cette instruction est à taper dans votre terminal et le caractère `%` représente votre [invite de commande](https://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande) (probablement du type `nom.prenom.etu@votre_machine` en salle de TP).

Par exemple l'aide en ligne s'invoque comme suit

```
% git help
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
               [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
               [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
               [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
               <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone     Clone a repository into a new directory
   init      Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add       Add file contents to the index
   mv        Move or rename a file, a directory, or a symlink
   restore   Restore working tree files
   rm        Remove files from the working tree and from the index

examine the history and state (see also: git help revisions)
   bisect    Use binary search to find the commit that introduced a bug
   diff      Show changes between commits, commit and working tree, etc
   grep      Print lines matching a pattern
   log       Show commit logs
   show      Show various types of objects
   status    Show the working tree status

grow, mark and tweak your common history
   branch    List, create, or delete branches
   commit    Record changes to the repository
   merge     Join two or more development histories together
   rebase    Reapply commits on top of another base tip
   reset     Reset current HEAD to the specified state
   switch    Switch branches
   tag       Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch     Download objects and refs from another repository
   pull      Fetch from and integrate with another repository or a local branch
   push      Update remote refs along with associated objects

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
See 'git help git' for an overview of the system.
```

Remarquez que les commandes sont bavardes et en lisant la sortie standard, on sait généralement ce qu'il convient de faire.

Autre exemple pour voir et manipuler les variables de git :
~~~
% git config -l
    user.name=Sedoglavic Alexandre
    user.email=alexandre.sedoglavic@univ-lille.fr
~~~

### 2.2 Créons notre premier dépôt local

Notre dépôt sera consacré aux astres de notre système solaire et à l'évolution de notre compréhension de ce dernier.

#### 2.2.1 Créons les données

Pour ce faire, commençons par créer un répertoire de travail (disons dans le répertoire `/tmp`)
~~~
% cd /tmp/
% mkdir NotreSystemeSolaire
~~~

et faisons en notre répertoire courant
~~~
% cd NotreSystemeSolaire
~~~

Commençons par créer un répertoire data pour contenir les données de certains astres actuellement connus (en 2023).
~~~
% mkdir data
~~~
Et remplissons le avec les pages wikipédia associées à différents astres avec les commandes 
~~~
% curl https://fr.wikipedia.org/wiki/Mercure_\(plan%C3%A8te\) > data/Mercure
% curl https://fr.wikipedia.org/wiki/V%C3%A9nus_\(plan%C3%A8te\) > data/Venus
% curl https://fr.wikipedia.org/wiki/Terre_\(plan%C3%A8te\) > data/Terre
% curl https://fr.wikipedia.org/wiki/Mars_\(plan%C3%A8te\) > data/Mars
% curl https://fr.wikipedia.org/wiki/\(1\)_C%C3%A9r%C3%A8 > data/Ceres
% curl https://fr.wikipedia.org/wiki/\(4\)_Vesta > data/Vesta
% curl https://fr.wikipedia.org/wiki/\(2\)_Pallas > data/Pallas
% curl https://fr.wikipedia.org/wiki/\(10\)_Hygie > data/Hygie
% curl https://fr.wikipedia.org/wiki/Jupiter_\(plan%C3%A8te\) > data/Jupiter
% curl https://fr.wikipedia.org/wiki/Saturne_\(plan%C3%A8te\) > data/Saturne 
% curl https://fr.wikipedia.org/wiki/Uranus_\(plan%C3%A8te\) > data/Uranus
% curl https://fr.wikipedia.org/wiki/Neptune_\(plan%C3%A8te\) > data/Neptune
% curl https://fr.wikipedia.org/wiki/Pluton_\(plan%C3%A8te_naine\) > data/Pluton
% curl https://fr.wikipedia.org/wiki/\(90482\)_Orcus > data/Orcus
% curl https://fr.wikipedia.org/wiki/\(136108\)_Haum%C3%A9a > data/Haumea
% curl https://fr.wikipedia.org/wiki/\(136472\)_Mak%C3%A9mak%C3%A9 > data/Makemake
~~~
L'état de notre répertoire data est donc maintenant
~~~
% ls data
Ceres		Hygie		Makemake	Mercure		Orcus		Pluton		Terre		Venus
Haumea		Jupiter		Mars		Neptune		Pallas		Saturne		Uranus		Vesta
%
~~~
L'état de notre répertoire courant est donc maintenant
~~~
% ls -a
. .. data
%
~~~
Le répertoire `.` représente le répertoire courant, tandis que `..` est le répertoire le contenant. Ces deux répertoires ne sont indiqués par la commande `ls` que si l'on utilise l'option `-a`.
Notre répertoire n'est pas encore un dépôt git.

#### 2.2.1 Créons les métadonnées

Pour initialiser un dépôt local à partir de ce répertoire du système de fichiers, on utilise :
~~~
% git init
hint: Using 'main' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint: 	git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'main' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint: 	git branch -m <name>
Initialized empty Git repository in /tmp/NotreSystemeSolaire/.git/
~~~

Ce faisant, nous venons de créer des méta-données utilisées par git dans un nouveau répertoire (indiqué par la commande `ls` que si on utilise l'option `-a`).
~~~
% ls -a
.	..	.git data 
% ls
data
%
~~~
(nous n'avons pas à nous préoccuper de ce répertoire .git, c'est le boulot du gestionnaire de version et des commandes que nous allons voir par la suite).
Notre répertoire est maintenant un dépôt git constitué de données et de méta-données.

### 2.3. Cycles d'évolutions du dépôt : la création d'un commit

Pour enregistrer une nouvelle version dans le dépôt git, on passe couramment par les étapes décrites dans cette section. 

#### 2.3.1 Consultons le journal des versions

Avec la commande `git log`, nous pouvons maintenant consulter l'ensemble des versions constituant la branche courante.
~~~
% git log
fatal: your current branch 'main' does not have any commits yet
~~~
Ce qui est normal car nous n'avons créé aucun commit (*i.e.* enregistré aucune version dans la branche courante et donc dans les méta-données) bien que notre répertoire contient des données, elles ne font pas encore partie du dépôt (elles n'ont pas été prises en compte par le gestionnaire git). 

#### 2.3.2 Consultons l'état actuel de l'espace de travail

Notre répertoire constitue l'espace de travail et on peut le distinguer en trois parties :
1. la branche constituée des versions
2. les fichiers suivis par git 
3. les fichiers non suivis par git

~~~ 
% git status
On branch main 

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	data/

nothing added to commit but untracked files present (use "git add" to track)
%
~~~
Notre espace de travail n'a donc aucun commit (la branche `main` est vide), rien n'est modifié et le répertoire `data` n'est pas suivi.
 
Un répertoire peut donc contenir des informations non prises en compte par le dépôt qui lui est associé.

Pour faire prendre en compte certaines données dans le dépôt, il faut l'indiquer explicitement par des commandes permettant à terme de créer une version.

#### 2.3.3 Préparons notre prochaine version en ajoutant ce que nous voulons prendre en compte

Nous pouvons commencer notre parcours historique.

Six planètes (Mercure, Vénus, la Terre, Mars, Jupiter et Saturne) sont connues depuis l’antiquité.

Nous pouvons donc créer un répertoire `Planetes` pour refléter ce fait et déplaçons y les fichiers associés à ces planètes.
~~~
% mkdir Planetes
% mv data/Mercure Planetes
% mv data/Venus Planetes
% mv data/Terre Planetes
% mv data/Mars Planetes
% mv data/Jupiter Planetes
% mv data/Saturne Planetes
~~~
Maintenant, l'état de notre espace de travail est
~~~
% git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	Planetes/
	data/

nothing added to commit but untracked files present (use "git add" to track)
~~~

En suivant les commentaires de cette commande, nous pouvons faire passer le répertoire `Planetes` (et les fichiers qu'il contient) de l'état non suivi à l'état suivi.

~~~
% git add Planetes 
% git status
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   Planetes/Jupiter
	new file:   Planetes/Mars
	new file:   Planetes/Mercure
	new file:   Planetes/Saturne
	new file:   Planetes/Terre
	new file:   Planetes/Venus

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	data/
~~~
Mais cela n'a rien changé concernant les versions, notre branche est toujours vide.

Nous sommes prêt à construire notre premier commit (*a.k.a.* notre première version).

#### 2.3.4 Entérinons l'état courant comme une version 

Pour ce faire, on utilise la commande suivante
~~~
% git commit -m "Planètes connues depuis l'antiquité"
[main (root-commit) 70c5b13] Planètes connues depuis l'antiquité
 6 files changed, 5898 insertions(+)
 create mode 100644 Planetes/Jupiter
 create mode 100644 Planetes/Mars
 create mode 100644 Planetes/Mercure
 create mode 100644 Planetes/Saturne
 create mode 100644 Planetes/Terre
 create mode 100644 Planetes/Venus
%
~~~
**{+remarque+}** Si vous n'utilisez pas l'option `-m`, la commande `git commit` vous ouvre l'éditeur de texte par défaut que vous ne maîtrisez pas forcément, autant éviter cette situation.

Nous venons de créer notre premier commit comme l'indique le journal de version
~~~
% git log
commit 70c5b13a687ac52ce526055ebc79415ccf203ae5 (HEAD -> main)
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 17:26:52 2023 +0200

    Planètes connues depuis l'antiquité
~~~

De plus, notre espace de travail a été modifié comme l'indique la commande
~~~
% git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	data/

nothing added to commit but untracked files present (use "git add" to track)
~~~
Il n'y a plus de proposition de modification de la version courante que l'on pourrait prendre en compte pour en construire une nouvelle.

**{+remarque+}** Nous laisserons le répertoire `data` dans notre espace de travail hors de notre dépôt censé représenter l'évolution de la compréhension de notre système solaire. 

### 2.4 Bis repetitas

Nous pouvons maintenant retracer l'histoire de la compréhension de notre système solaire pour nous entraîner à faire des commits.

#### 2.4.1 Le hasard fait bien les choses
La planète Uranus a été découverte au télescope par Williams Herschel en 1781.

Nous pouvons donc ajouter le fichier correspondant au répertoire `Planetes`
~~~
% mv data/Uranus Planetes/
~~~
et vérifier l'état de notre espace de travail
~~~
 % git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	Planetes/Uranus
	data/

nothing added to commit but untracked files present (use "git add" to track)
~~~
Pour de nouveau préparer l'ajout du fichier correspondant à Uranus à notre prochain commit
~~~
% git add Planetes/Uranus 
% git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   Planetes/Uranus

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	data/
~~~
et construire la nouvelle version
~~~
% git commit -m "Découverte d'Uranus par Herschel"
[main 1cf3c96] Découverte d'Uranus par Herschel
 1 file changed, 477 insertions(+)
 create mode 100644 Planetes/Uranus
~~~
Vérifions en consultant le journal des versions
~~~
% git log
commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9 (HEAD -> main)
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 17:51:18 2023 +0200

    Découverte d'Uranus par Herschel

commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 17:26:52 2023 +0200

    Planètes connues depuis l'antiquité
~~~

#### 2.4.2 Quand une relation empirique porte le nombre de planètes à 10  

Pour confirmer la relation de Titius-Bode, des astronomes se sont mis en 1800 à la recherche d’une planète entre Mars et Jupiter. 

Ainsi en 1801, ont été observés Cérès, Vesta et Pallas. 

Ces découvertes ont donc porté le nombre de planètes de notre système solaire à dix planètes. 

Maintenant, le process de construction de nouvelle version nous est habituel et nous nous passons de vérifier l'état de notre espace de travail (`git status`).
~~~
% mv data/Ceres Planetes
% mv data/Vesta Planetes
% git add Planetes/Ceres Planetes/Vesta 
% git commit -m "La chasse aux planètes entre Mars et Jupiter"
[main 5b422ee] La chasse aux planètes entre Mars et Jupiter
 2 files changed, 1715 insertions(+)
 create mode 100644 Planetes/Ceres
 create mode 100644 Planetes/Vesta
~~~
Pour obtenir la branche
~~~
% git log
commit 5b422ee25dc9e5ba0a6b543ce77305a3f28d373d (HEAD -> main)
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 18:01:05 2023 +0200

    La chasse aux planètes entre Mars et Jupiter

commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 17:51:18 2023 +0200

    Découverte d'Uranus par Herschel

commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
Date:   Fri Oct 13 17:26:52 2023 +0200

    Planètes connues depuis l'antiquité
~~~

Il est possible de corriger une erreur sur un commit facilement si c'est le dernier en date.

Vous avez sans doute remarqué que l'auteur de ces lignes a oublié d'ajouter Pallas au répertoire `Planetes`.
Après avoir changé l'état de l'espace de travail, nous pouvons --- uniquement si c'est le dernier en date --- amender notre notre commit pour corriger cela
~~~
% mv data/Pallas Planetes/
% git add Planetes/Pallas
% git commit --amend --no-edit
~~~

#### 2.4.3 Trop de petites planètes entre Mars et Jupiter 

Devant les découvertes d’Hygie et d’autres corps célestes qui se sont succédés, Herschel a introduit la notion d’astéroı̈de pour éviter d’avoir à considérer un trop grand nombre de planètes de tailles trop dissemblables dans notre système solaire (certaines sources de l'époque comptaient 33 planètes dans notre système solaire). 

Nous allons donc créer un répertoire `Asteroides` et placer les fichiers correspondant dedans
~~~
% mkdir Asteroides
% git mv Planetes/Ceres Asteroides
% git mv Planetes/Pallas Asteroides
% git mv Planetes/Vesta Asteroides
% mv data/Hygie Asteroides
% git status 
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    Planetes/Ceres -> Asteroides/Ceres
	renamed:    Planetes/Pallas -> Asteroides/Pallas
	renamed:    Planetes/Vesta -> Asteroides/Vesta

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	Asteroides/Hygie
	data/
~~~

**{+remarque+}** Pour déplacer (`mv`) un fichier affecté par une version (disons Pallas) il faut utiliser `git mv`.

Le fichier associé à Hygie ne nécessite pas ce traitement mais doit être suivi pour faire partie du prochain commit.

~~~
% git add Asteroides/Hygie
% git commit -m "Création de la notion d'astéroide"
[main e6c5a7e] Création de la notion d'astéroide
 4 files changed, 1127 insertions(+)
 rename {Planetes => Asteroides}/Ceres (100%)
 create mode 100644 Asteroides/Hygie
 rename {Planetes => Asteroides}/Pallas (100%)
 rename {Planetes => Asteroides}/Vesta (100%)
~~~

Nous voici avec 4 gros astéroïdes et 7 planètes

~~~
% ls Asteroides | nl
     1	Ceres
     2	Hygie
     3	Pallas
     4	Vesta
% ls Planetes | nl  
     1	Jupiter
     2	Mars
     3	Mercure
     4	Saturne
     5	Terre
     6	Uranus
     7	Venus
~~~

#### 2.4.4  Et 2 nouvelles planètes

La planète Neptune a été localisé calculatoirement par Urbain Le Verrier en 1846 et observé par Johann Gottfried Galle (John Couch Adams est arrivé aux mêmes conclusions en 1843 mais n'a pas réussi à convaincre l'observatoire de Cambridge de l'observer). 
~~~
% mv data/Neptune Planetes 
% git add Planetes/Neptune 
% git commit -m "Découverte de Neptune par Le Verrier/Galle"
[main 47b1cc4] Découverte de Neptune par Le Verrier/Galle
 1 file changed, 477 insertions(+)
 create mode 100644 Planetes/Neptune
~~~

Pluton a été découverte par Clive Tombaugh en 1930 et considérée comme une planète. 
~~~
% mv data/Pluton Planetes 
% git add Planetes/Pluton 
% git commit -m "Découverte de Pluton par Tombaugh"
[main 08f479f] Découverte de Pluton par Tombaugh
 1 file changed, 477 insertions(+)
 create mode 100644 Planetes/Pluton
~~~

#### 2.4.5 On perd une planète mais on gagne des planètes naines

À partir de 2005, les astronomes ont découvert d'autres corps similaires à Pluton (Makémaké, Hauméa, etc). 

Ceci a motivé une nouvelle définition de la notion de planètes et l’introduction de la notion de planète naine. 

Pluton et Cérès qui ne sont pas solitaires sur leurs orbites sont des planètes naines mais ni Pallas ni Vesta car elles ne sont pas en équilibre hydrostatique (*i.e.* ronde).

Nous pouvons répercuter ces informations sur notre dépôt.
~~~
% mkdir PlanetesNaines
% git mv Planetes/Pluton PlanetesNaines/Pluton
% git mv Asteroides/Ceres PlanetesNaines/
% mv data/Orcus Asteroides/
% mv data/Haumea PlanetesNaines/
% mv data/Makemake PlanetesNaines/
% git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    Asteroides/Ceres -> PlanetesNaines/Ceres
	renamed:    Planetes/Pluton -> PlanetesNaines/Pluton

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	Asteroides/Orcus
	PlanetesNaines/Haumea
	PlanetesNaines/Makemake
~~~

Et faire le nécessaire pour faire un dernier commit
~~~
% git add Asteroides/Orcus PlanetesNaines/Haumea PlanetesNaines/Makemake 
% git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   Asteroides/Orcus
	renamed:    Asteroides/Ceres -> PlanetesNaines/Ceres
	new file:   PlanetesNaines/Haumea
	new file:   PlanetesNaines/Makemake
	renamed:    Planetes/Pluton -> PlanetesNaines/Pluton
% git commit -m "Introduction de la notion de planète naine"
[main 8c0e57c] Introduction de la notion de planète naine
 5 files changed, 2502 insertions(+)
 create mode 100644 Asteroides/Orcus
 rename {Asteroides => PlanetesNaines}/Ceres (100%)
 create mode 100644 PlanetesNaines/Haumea
 create mode 100644 PlanetesNaines/Makemake
 rename {Planetes => PlanetesNaines}/Pluton (100%)
~~~

Au final, nous avons donc la situation
~~~
% ls
Asteroides	Planetes	PlanetesNaines	data
% ls Asteroides | nl
     1	Hygie
     2	Orcus
     3	Pallas
     4	Vesta
% ls PlanetesNaines | nl
     1	Ceres
     2	Haumea
     3	Makemake
     4	Pluton
% ls Planetes | nl
     1	Jupiter
     2	Mars
     3	Mercure
     4	Neptune
     5	Saturne
     6	Terre
     7	Uranus
     8	Venus
% ls data
% 
~~~

### 2.5 Déplacement sur une branche

Nous venons de construire un dépôt avec une seule branche
~~~
% git branch                
* main
~~~
donc on peut consulter l'ensemble des versions sur cette branche
~~~
% git log --graph
* commit 8c0e57c1b1362928c6366d7a3f2f3fb550120b4c (HEAD -> main)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 19:10:26 2023 +0200
| 
|     Introduction de la notion de planète naine
| 
* commit 08f479f594a484985562661bd7dbb8eb13994bdf
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:57:30 2023 +0200
| 
|     Découverte de Pluton par Tombaugh
| 
* commit 47b1cc4bf364083b946e9cb984b6fc695a017952
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:55:12 2023 +0200
| 
|     Découverte de Neptune par Le Verrier/Galle
| 
* commit e6c5a7eea75cab027c6eed85f80c05a1bbd74d48
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:44:55 2023 +0200
| 
|     Création de la notion d'astéroide
| 
* commit 7164636d124b646f5d1e78b896547ff48f24e28c
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
~~~

Les *hash* d'un commit sont les numéros hexadécimaux qui correspondent aux références utilisées pour désigner ces versions (70c5b13a687ac52ce526055ebc79415ccf203ae5 pour le tout premier par exemple).

**{+remarque+}** Les hash sont propres à chaque commit et donc diffèrent d'un dépôt
local à un autre.

Ces numéros ne sont pas pratiques à utiliser et donc on peut associer à ces derniers des étiquettes (*tag*).

Une de ces étiquettes est *HEAD* qui se positionne automatiquement sur le commit courant~:

#### 2.5.1 Revenir à une version

Les hash permettant de désigner un commit sont utilisés pour se déplacer sur une branche avec la commande suivante
~~~
% git checkout 7164636d124b646f5d1e78b896547ff48f24e28c
Note: switching to '7164636d124b646f5d1e78b896547ff48f24e28c'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 7164636 La chasse aux planètes entre Mars et Jupiter
% git log --graph
* commit 7164636d124b646f5d1e78b896547ff48f24e28c (HEAD)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
~~~
L'étiquette HEAD est maintenant au milieu de la branche et plus sur son extrémité ('detached HEAD' state).

Nous sommes maintenant revenu à l'état du dépôt correspondant à la découverte des planètes entre Mars et Jupiter.
~~~
% ls
Planetes	data
% ls Planetes | nl
     1	Ceres
     2	Jupiter
     3	Mars
     4	Mercure
     5	Pallas
     6	Saturne
     7	Terre
     8	Uranus
     9	Venus
    10	Vesta
% ls data
% 
~~~
#### 2.5.2 Revenir sur le bout de la branche 

Pour revenir au bout de la branche, on peut utiliser
~~~
% git checkout main
% git checkout main
Previous HEAD position was 7164636 La chasse aux planètes entre Mars et Jupiter
Switched to branch 'main'
~~~
Et nous sommes revenus dans la version associée. 
~~~
% git log --graph
* commit 8c0e57c1b1362928c6366d7a3f2f3fb550120b4c (HEAD -> main)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 19:10:26 2023 +0200
| 
|     Introduction de la notion de planète naine
| 
* commit 08f479f594a484985562661bd7dbb8eb13994bdf
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:57:30 2023 +0200
| 
|     Découverte de Pluton par Tombaugh
| 
* commit 47b1cc4bf364083b946e9cb984b6fc695a017952
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:55:12 2023 +0200
| 
|     Découverte de Neptune par Le Verrier/Galle
| 
* commit e6c5a7eea75cab027c6eed85f80c05a1bbd74d48
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:44:55 2023 +0200
| 
|     Création de la notion d'astéroide
| 
* commit 7164636d124b646f5d1e78b896547ff48f24e28c
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
% ls
Asteroides	Planetes	PlanetesNaines	data
% ls Asteroides | nl
     1	Hygie
     2	Orcus
     3	Pallas
     4	Vesta
% ls PlanetesNaines | nl
     1	Ceres
     2	Haumea
     3	Makemake
     4	Pluton
% ls Planetes | nl
     1	Jupiter
     2	Mars
     3	Mercure
     4	Neptune
     5	Saturne
     6	Terre
     7	Uranus
     8	Venus
% ls data
% 
~~~

Toutes les étapes de nos manipulations ont été conservées (efficacement) et nous pouvons revenir à ces versions pour retravailler à partir d'elles si nécessaires.

### 2.6 Les cycles d'évolution s'appliquent aussi aux modifications des fichiers

Nous avons illustré `git status`, `git add`, `git commit` sur des déplacements de fichiers.

Git permet ces manipulations sur l'édition de fichier.

Nous allons illustrer ce point tout en créant une nouvelle branche issue de `main`.

#### 2.6.1 Création d'une nouvelle branche

Pour commencer plaçons nous de nouveau dans l'état correspondant à un système solaire composé de 10 planètes.

~~~ 
% git checkout 7164636d124b646f5d1e78b896547ff48f24e28c
Note: switching to '7164636d124b646f5d1e78b896547ff48f24e28c'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 7164636 La chasse aux planètes entre Mars et Jupiter
~~~

Notre étiquette HEAD correspondant au commit courant est détachée (*i.e.* elle n'est plus sur un bout de branche).

Nous allons créer une nouvelle branche --- disons secondary --- à partir de la branche main à cet endroit.

~~~
% git checkout -b secondary
Switched to a new branch 'secondary'
% git branch
  main
* secondary
~~~
Et nous pouvons constater que l'étiquette `HEAD` pointe sur une branche `secondary` qui a trois commit en commun avec la branche main
~~~
% git log --all --graph
* commit 8c0e57c1b1362928c6366d7a3f2f3fb550120b4c (main)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 19:10:26 2023 +0200
| 
|     Introduction de la notion de planète naine
| 
* commit 08f479f594a484985562661bd7dbb8eb13994bdf
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:57:30 2023 +0200
| 
|     Découverte de Pluton par Tombaugh
| 
* commit 47b1cc4bf364083b946e9cb984b6fc695a017952
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:55:12 2023 +0200
| 
|     Découverte de Neptune par Le Verrier/Galle
| 
* commit e6c5a7eea75cab027c6eed85f80c05a1bbd74d48
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:44:55 2023 +0200
| 
|     Création de la notion d'astéroide
| 
* commit 7164636d124b646f5d1e78b896547ff48f24e28c (HEAD -> secondary)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
~~~

### 2.6.2 Modification du fichier Planetes/Pallas

Soit avec un éditeur de texte, soit avec une ligne de commande comme ci-dessous, on peut modifier le fichier Pallas dans le répertoire `Planetes`.
~~~
% grep reclassification Planetes/Pallas  
À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
% sed -i -e "s/reclassification/reclassification en 1802/" Planetes/Pallas 
% grep reclassification Planetes/Pallas                                 
À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification en 1802. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
% 
~~~
Git se rend compte de cette modification
~~~
% git status          
On branch secondary
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   Planetes/Pallas

no changes added to commit (use "git add" and/or "git commit -a")
~~~
Et on peut même lui demander ce qui a été modifié
~~~
% git diff
diff --git a/Planetes/Pallas b/Planetes/Pallas
index 852d6e7..cc5fe85 100644
--- a/Planetes/Pallas
+++ b/Planetes/Pallas
@@ -856,7 +856,7 @@ A802 FA</td>
-À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
+À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification en 1802. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
~~~
Si cette modification nous convient, on peut la placer dans les modifications qui vont constituer la prochaine version
~~~
% git add Planetes/Pallas
% git status  
On branch secondary
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	modified:   Planetes/Pallas
~~~
Il n'y aura plus alors de différence entre l'état de l'espace de travail et ce qui va être versionné.
~~~
% git diff
~~~
Il ne reste plus qu'à faire une nouvelle version
~~~
% git commit -m "Ajout d'une précision dans un fichier"
[secondary 50d476c] Ajout d'une précision dans un fichier
 1 file changed, 1 insertion(+), 1 deletion(-)
~~~
Et maintenant nos branches `secondary` et `main` ont divergé.
~~~
% git log --all --graph                              
* commit 50d476c097993597c4221ead6e30b9bb71d3db42 (HEAD -> secondary)
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 19:59:40 2023 +0200
| 
|     Ajout d'une précision dans un fichier
|   
| * commit 8c0e57c1b1362928c6366d7a3f2f3fb550120b4c (master)
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 19:10:26 2023 +0200
| | 
| |     Introduction de la notion de planète naine
| | 
| * commit 08f479f594a484985562661bd7dbb8eb13994bdf
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 18:57:30 2023 +0200
| | 
| |     Découverte de Pluton par Tombaugh
| | 
| * commit 47b1cc4bf364083b946e9cb984b6fc695a017952
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 18:55:12 2023 +0200
| | 
| |     Découverte de Neptune par Le Verrier/Galle
| | 
| * commit e6c5a7eea75cab027c6eed85f80c05a1bbd74d48
|/  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
|   Date:   Fri Oct 13 18:44:55 2023 +0200
|   
|       Création de la notion d'astéroide
| 
* commit 7164636d124b646f5d1e78b896547ff48f24e28c
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
~~~
Si on revient sur le bout de la branche `main` (`git checkout main`) cette modification aura disparu.
~~~
% git checkout main
% grep reclassification */Pallas
À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
~~~
Mais en revenant à la branche `secondary`, cette modification est retrouvée.
~~~
% git checkout secondary
Switched to branch 'secondary'
% grep reclassification */Pallas
À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification en 1802. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
~~~

#### 2.6.3 Fusion de branches : on veut la modification du fichier Pallas dans la version finale du dépôt.

Les branches peuvent être fusionnées suivant le schéma suivant :
1. On se positionne sur la branche qui doit contenir le résultat de la fusion
2. On effectue la fusion en indiquant la branche à fusionner avec la branche courante
Ce qui se fait comme suit dans notre cas.
~~~
% git checkout main
% git merge --no-edit secondary
~~~
Et on obtient donc le dépôt :
~~~
% git log --all --graph
*   commit cd71103773e23c280c5c69fd8b503dd590ef2333 (HEAD -> master)
|\  Merge: 8c0e57c 50d476c
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 20:05:40 2023 +0200
| | 
| |     Merge branch 'secondary'
| | 
| * commit 50d476c097993597c4221ead6e30b9bb71d3db42 (secondary)
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 19:59:40 2023 +0200
| | 
| |     Ajout d'une précision dans un fichier
| | 
* | commit 8c0e57c1b1362928c6366d7a3f2f3fb550120b4c
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 19:10:26 2023 +0200
| | 
| |     Introduction de la notion de planète naine
| | 
* | commit 08f479f594a484985562661bd7dbb8eb13994bdf
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 18:57:30 2023 +0200
| | 
| |     Découverte de Pluton par Tombaugh
| | 
* | commit 47b1cc4bf364083b946e9cb984b6fc695a017952
| | Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| | Date:   Fri Oct 13 18:55:12 2023 +0200
| | 
| |     Découverte de Neptune par Le Verrier/Galle
| | 
* | commit e6c5a7eea75cab027c6eed85f80c05a1bbd74d48
|/  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
|   Date:   Fri Oct 13 18:44:55 2023 +0200
|   
|       Création de la notion d'astéroide
| 
* commit 7164636d124b646f5d1e78b896547ff48f24e28c
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 18:01:05 2023 +0200
| 
|     La chasse aux planètes entre Mars et Jupiter
| 
* commit 1cf3c96ed66d2be21105ea9246e4eb29f4135fd9
| Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
| Date:   Fri Oct 13 17:51:18 2023 +0200
| 
|     Découverte d'Uranus par Herschel
| 
* commit 70c5b13a687ac52ce526055ebc79415ccf203ae5
  Author: Sedoglavic <alexandre.sedoglavic@univ-lille.fr>
  Date:   Fri Oct 13 17:26:52 2023 +0200
  
      Planètes connues depuis l'antiquité
~~~

avec l'état
~~~
% ls -R
Asteroides	Planetes	PlanetesNaines	data

./Asteroides:
Hygie	Orcus	Pallas	Vesta

./Planetes:
Jupiter	Mars	Mercure	Neptune	Saturne	Terre	Uranus	Venus

./PlanetesNaines:
Ceres		Haumea		Makemake	Pluton

./data:
~~~
et la modification du fichier Pallas issue de la branche `secondary`
~~~
% grep reclassification Asteroides/Pallas 
À l'instar de <a href="/wiki/(1)_C%C3%A9r%C3%A8s" title="(1) Cérès">Cérès</a>, <a href="/wiki/(3)_Junon" title="(3) Junon">Junon</a> et <a href="/wiki/(4)_Vesta" title="(4) Vesta">Vesta</a>, il fut considéré comme une <a href="/wiki/Plan%C3%A8te" title="Planète">planète</a> jusqu'à ce que la découverte de nombreux autres astéroïdes conduise à sa reclassification en 1802. Comme celle de Pluton, l'orbite de Pallas est très fortement inclinée (34,8°) par rapport au plan de la ceinture d'astéroïdes principale, ce qui rend l'astéroïde difficilement accessible par engin spatial. 
~~~

#### 2.6.3 Des conflits peuvent survenir entre les différentes branches lors des fusions 

Dans l'exemple précédent, les modifications sur les branches `main` et `secondary` sont totalement indépendantes et donc leur fusion peut se faire sans intervention humaine.

Supposons qu'avant la fusion, les branches `main` et `secondary` avaient chacune une version d'un fichier `E.txt` incompatibles.

Dans ce cas, on aurait obtenu un message
~~~
    Auto-merging E.txt
    CONFLICT (add/add): Merge conflict in E.txt
    Automatic merge failed; fix conflicts and then commit the result.
    % cat E.txt 
    <<<<<<< HEAD
    La version sur la branche main
    =======
    La version de la branche secondary
    >>>>>>> secondary
~~~
La partie entre chevrons (<<< et >>>) indique les conflits. 

En choisissant une version, en supprimant l'autre et les indication (<<<, === et >>>), on peut construire une nouvelle version sans conflit.
~~~
    % sed -i '1,3d' E.txt ; sed -i '2d' E.txt 
    % git add E.txt
    % git commit -m "<fix> resolve conflict on E.txt"
    [main 3bb4255] <fix> resolve conflict on E.txt
    %
~~~

#### 2.6.4 Idéalement

La branche main devrait toujours être propre et fonctionnelle (l’ensemble des tests passent).

Pour faire une modification :
-   on construit une nouvelle branche à partir de main ;
-   on développe et on teste sur cette branche jusqu'à l'obtention d'un état stable, fonctionnel et testé ;
-   on fait passer une revue de cette branche par un tiers qui *merge* avec la branche main.

De très nombreuses méthodologies de développement existent et une première étape dans la collaboration consiste à se mettre d’accord sur celle utilisée.

## 3. Écosystème Git

### 3.1. Dépôts local et distant

#### 3.1.1 Désignation
Pour lister l’ensemble des dépôts distants en relation avec le dépôts local, on peut utiliser git remote dans votre dépôt local.
~~~
% git remote -v
%
~~~

Vous n'en avez pas pour le dépôt local construit dans les sections précédentes (vous pourrez expérimenter une association entre dépôts local et distant plus loin dans la suite après avoir créé un dépôt distant avec gitlab).

Par exemple, le dépôt distant des supports du cours que vous suivez est:
~~~
% git remote -v
origin	git@gitlab-fil.univ-lille.fr:ls1-odi/portail.git (fetch)
origin	git@gitlab-fil.univ-lille.fr:ls1-odi/portail.git (push)
~~~

Le dépôt distant est nommé dans ce cas `origin`.

### 3.1.2 Interaction entre local et distant

Pour "pousser" (déposer) son travail du dépôt local vers le dépôt distant :

~~~
% git push origin main
~~~

Pour "tirer" (extraire) le contenu du dépôt distant vers le dépôt local :
~~~
% git pull origin main
~~~

Nous allons voir comment faire un dépôt distant en utilisant le serveur [gitlab.univ-lille.fr](https://gitlab.univ-lille.fr/) auquel vous avez accès.

### 3.2  Gitlab

#### 3.2.1 L'environnement Gitlab

[Gitlab](https://fr.wikipedia.org/wiki/GitLab) est une application web basée sur git qui permet de gérer :

-   le cycle de vie de projets git ;

-   les participants aux projets (droits, groupes, *etc.*) ;

-   la communication entre ces participants (tickets, *etc.*) ;

-   construction, test et déploiement automatiques.

C'est un produit d'une société privée dont la "community edition" est libre.

#### 3.2.2  Créer et partager un dépôt distant

**{-à faire-}** Ouvrez le serveur [gitlab.univ-lille.fr](https://gitlab.univ-lille.fr/) dans votre navigateur.

Une fois connecté avec votre compte ULille, GitLab permet de créer un nouveau dépôt en cliquant sur *New project*:

![Gitlab-ULille new project](./img/new-project.png)

![Gitlab-ULille new blank project](./img/new-blank-project.png)

Un identifiant est nécessaire et vous l'indiquez par le biais de *Project name*.

Pour vos travaux dans l'<span class="smallcaps">ue</span>, vous devrez cocher qu'il s'agit d’un projet *Private*.

Pour finir, validez en cliquant sur *Create project*.

![Gitlab-ULille create new blank project](./img/new-blank-project-create.png)

**{+remarque+}** Si vous souhaitez créer un dépôt complètement vide qui ne contient aucun commit, il faut décocher la case *Initialize repository with a README*, ce qui vous permettra de remplir ce dépôt à partir d'un dépôt local déjà existant :

![Gitlab-Ulille empty project](./img/create-empty-project.png)

Vous pouvez donner des droits à votre enseignant et à votre binôme sur ce dépôt pour qu'ils puissent accéder et/ou contribuer au code (il faut que votre binôme se soit connecté au moins une fois à GitLab).

Pour ce faire, cliquez sur le menu *Manage* - *Members* (en haut à droite) renseignez l’identifiant de votre binôme (son *login*), changez les droits d’accès pour *Owner*, puis cliquez sur l'icône *Invite* :

![Gitlab-ULille members](./img/members.png)

![Gitlab-ULille invite members](./img/invite-members.png)

![Gitlab-ULille invite members role](./img/invite-members-role.png)

### 3.3 Forker à partir d'un dépôt existant

Vos enseignants partageront probablement des dépôt avec vous.

Pour ce faire, ils vous communiqueront l'<span class="smallcaps">url</span> d'un dépôt distant préexistant.

Dans votre espace gitlab, après avoir saisi cette <span class="smallcaps">url</span> vous devrez la *forker* en cliquant sur l’icône *fork*.

![Gitlab-ULille fork](./img/fork.png)

L’interface gitlab vous proposera alors un domaine de nom (namespace) dans lequel effectuer le fork (cela conditionne vos droits sur ce nouveau dépôt).

Ceci fait, vous venez d’obtenir un nouveau dépôt distant que vous pourrez ensuite cloner, *etc.*

<!-- Généralement, il conviendra de supprimer la dépendance entre le dépôt que vous venez de forker et le dépôt initial.

Pour ce faire, vous devez naviguer dans la configuration avancée (*settings* - *advanced* - *remove*) lors du fork. -->

### 3.4 Première synchronisation du dépôt

### 3.4.1 Remplir un dépôt distant vide à partir d'un dépôt local existant

Le procédé plus simple pour remplir un dépôt distant à partir d'un dépôt local déjà initialisé par la commande `git init` est de créer
d'abord un dépôt distant vide. Comme remarqué précédemment, pour la création d'un dépôt vide sur gitlab il faut décocher la case indiquée ci-dessous :

![Gitlab-Ulille empty project](./img/create-empty-project.png)

Cela permet de créer un dépôt sans aucun contenu ni commit :

![Gitlab-Ulille empty project](./img/empty-project.png)

**{+remarque+}** La page principale du projet vide montre les instructions à suivre pour mettre à jour ce dépôt selon la situation :

![Gitlab-Ulille empty project](./img/empty-project-instructions.png)

Le projet est associé à deux <span class="smallcaps">url</span> qui permettent de l'utiliser à distance. Ces <span class="smallcaps">url</span> 
sont disponibles en cliquant le bouton *Clone* :

![Gitlab-ULille empty project url](./img/empty-project-url.png)

On peut utiliser soit le protocole <span class="smallcaps">ssh</span> (nécessitant d’avoir votre clef privée en local et votre clef publique sur Gitlab) ou <span class="smallcaps">http</span> (nécessitant votre couple login/password de Gitlab).

Les icônes à droite de chaque <span class="smallcaps">url</span> permettent de coller l'adresse correspondante dans le presse-papier.

Pour publier sur ce dépôt gitlab vide un dépôt local existant déjà initialisé par `git init`, il faut utiliser **une et une seule fois** les commandes ci-dessous :

~~~
% git remote add origin URL-du-dépôt-git
% git push --set-upstream origin main
~~~

Cela permet de relier le dépôt local existant et le dépôt distant vide, ainsi que de synchroniser les deux. Une fois réalisée cette operation,
pour la synchronisation des deux dépôts il suffira d'utiliser les commandes `git pull` et `git push` mentionnées précédemment.

### 3.4.2 Construire un dépôt local à partir d'un dépôt distant

Pour construire votre dépôt local à partir du dépôt distant, vous devez cloner ce dernier toujours en utilisant son <span class="smallcaps">url</span> :

![Gitlab-ULille invite members](./img/project-url.png)

Une fois que vous avez obtenu l'<span class="smallcaps">url</span>, la commande `git clone` permet de créer la copie locale du dépôt :

~~~
% git clone URL-du-dépôt-git
Cloning into 'mon-premier-projet-git-odi'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (3/3), done.
~~~

<!-- Après avoir choisi un dépôt dans Gitlab, une icône en bas à droite permet de coller l’<span class="smallcaps">url</span> correspondante dans le presse-papier. -->

Comme vu précédemment, on peut utiliser soit le protocole <span class="smallcaps">ssh</span> ou <span class="smallcaps">http</span>.

Cette opération n'est à faire qu’une seule fois :

-   elle crée le dépôt local ;

-   en positionnant correctement les informations de `git remote` ;

-   ensuite on utilise `git pull`, `git push`.

### 3.4.3 Authentification via ssh ou http

**{+remarque+}** Lorsque la synchronisation du projet échoue pour des problèmes d'authentification, on se trouve
souvent dans un de deux cas de figure :
1. on utilise l'<span class="smallcaps">url</span> <span class="smallcaps">ssh</span> du dépôt sans avoir configuré une clef <span class="smallcaps">ssh</span> ;
2. on utilise l'<span class="smallcaps">url</span> <span class="smallcaps">http</span> du dépôt mais le serveur gitlab est configuré pour accepter 
seulement un *token* <span class="smallcaps">http</span> à la place de votre mot de passe.

Selon l'<span class="smallcaps">url</span> utilisé, il faudra donc ajouter à notre compte utilisateur soit 
une clef <span class="smallcaps">ssh</span> soit un token <span class="smallcaps">http</span>, 
via le menu accessible à partir de notre profil utilisateur :

![Gitlab-ULille user profile](./img/user-profile.png)

Pour ajouter une clef <span class="smallcaps">ssh</span> on selectionne l'entrée correspondante du menu :

![Gitlab-ULille add ssh key](./img/add-ssh-key.png)

![Gitlab-ULille add ssh key](./img/create-ssh-key.png)

La clef <span class="smallcaps">ssh</span> à ajouter au profil utilisateur est normalement 
contenue dans le fichier `~/.ssh/id_rsa.pub`, dont le contenu peut-être affiché au terminal 
à l'aide de la commande `cat` habituelle. Si ce fichier n'existe pas, il doit être d'abord
crée avec la commande `ssh-keygen`.

Pour ajouter un token d'authentification http, il faut selectionner l'entrée correspondante du menu du profil utilisateur :

![Gitlab-ULille add http token](./img/add-http-token.png)

Le token crée pourra être utilisé lors de l'authentification à la place de votre mot de passe.

**{+remarque+}** Pour permettre au token de fonctionner, il faut lui accorder au moins les droits de lecture et écriture du dépôt :

![Gitlab-ULille create http token](./img/create-http-token.png)

### 3.5 Synthèse minimale

Après avoir créé un dépôt distant puis l'avoir cloné en un dépôt local une seule fois sur votre espace <span class="smallcaps">nfs</span>, votre travail avec git peut se résumer à :

-   faire un `git pull` en début de séance ;

-   itérer autant que nécessaire le cycle :

    -   `git add`

    -   `git commit`

-   en fin de séance, il convient de pousser votre travail sur le dépôt distant par un `git push`.

Cet usage minimal est important car la plupart de vos <span class="smallcaps">tp</span> seront évalués à partir d'un dépôt git.

### 4 Partagez vos projets

**{-à faire-}** Créez un dépôt privé vide nommé `AAAA-ODI-GROUPE-MI-XY-Premier-projet-git` 
(où `AAAA` est à remplacer par l'année en cours, et XY par votre numéro de groupe) 
sur [gitlab.univ-lille.fr](https://gitlab.univ-lille.fr/), ajoutez votre binôme et le responsable de votre
groupe d'ODI aux membres du dépôt, et synchronisez votre dépôt local Planètes avec ce dépôt.

