## ODI – Outils pour le développement informatique

> [1re année de licence maths-info](https://portail.fil.univ-lille.fr/portail/index.php?dipl=L1_MI&sem=L1MIS1&ue=ACCUEIL), Univ. Lille
> année 2023/2024

Ce portail présente l'ensemble des ressources pédagogiques de l'UE ODI. 

[![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr) Sauf indication contraire, l’ensemble des contenus est mis à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/deed.fr).

## Travail pratique 1 — Découverte

Dans ce premier TP, vous allez découvrir les outils et notions de base qui vous
seront utiles pendant vos études de licence et en général lors de l'utilisation
d'un ordinateur.

Ce TP se compose de quatre sujets :
1. [La prise en main de l'environnement de travail](./1-env-graphique/README.md)
2. [Le système de fichiers et le terminal](./1-fs+terminal/README.md)
3. [L'editeur de texte Thonny](./1-thonny/README.md)
4. [Les notions de base de navigation web](./1-navigateur-web/README.md)

Pour aller plus loin :

5. [Le système de fichiers et le terminal - compléments](./1-fs+terminal/complements.md)

## Travail pratique 2 — Messagerie, agenda, et partage de fichiers

1. [Découverte de Zimbra, client de messagerie et de gestion de calendriers](2-zimbra/README.md)
1. [Partage de fichiers avec Nextcloud](2-nextcloud/readme.md)
   
## Travail pratique 3 — Produire des textes proprement

1. [Produire des documents avec Markdown](3-markdown/Readme.md)

Pour aller plus loin : 

2. [Écrire des formules mathématiques avec LaTeX](3-markdown/formules.md)
3. [Produire un diaporama avec Markdown](3-markdown/diapo.md)

## Intermède 

1. [Jouer au terminal](https://web.mit.edu/mprat/Public/web/Terminus/Web/main.html)

## Travail pratique 4 — Gestion collaborative de versions

1. [Introduction à _Git_ et _GitLab_](4-git/README.md)

## Intermède 

1. [Jouer avec Git](https://ohmygit.org/)

## Travail pratique 5 — Manipulation de fichiers de texte

1. [Outils de manipulation de fichiers de texte](5-outils-fichiers-texte/Readme.md)
2. [Scripts shell](5-scripts-shell/Readme.md)
